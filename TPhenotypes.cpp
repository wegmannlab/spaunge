/*
 * TPhenotypeStorage.cpp
 *
 *  Created on: Feb 19, 2016
 *      Author: wegmannd
 *		Modified : Blanquet Christophe
 */

// #define MEASURE_STEP_ITERATION
// #define WITHOUT_LOG
// #define WITHOUT_EXP

#include "TPhenotypes.h"

#include "TVcfFile.h"

#define FMATH_USE_XBYAK

#include "fmath.hpp"

#ifdef MEASURE_STEP_ITERATION
	#include <mach/clock.h>
	#include <mach/mach.h>
#endif

#ifdef MEASURE_STEP_ITERATION
	static double time_for_all_step_iterations = 0.0;
	static long nb_attempt_iteration = 0;
#endif


TPhenotypes::TPhenotypes(std::string & OutputName){
	//initialize some variables
	oneOverSqrtTwoPi = 1.0 / sqrt(2.0 * 3.1415926);
	genotypeFrequencies_M0 = new double[3];
	genotypeFrequencies_alt = new double[3];
	outputFilesOpened = false;
	meansVariancesInitialized = false;
	epsilonLL = 0.0001;
	epsilonF = 0.0001;
	outputName = OutputName;
	sampleNames = NULL;
	sampleNamesArrayCreated = false;
	storageCreated = false;

	//initialize all other variables
	numPhenotypes = 0;
	numSamples = 0;
	phenotypes = NULL;
	phenotypeNames = NULL;
	means = NULL;
	variances = NULL;
	genotypeLikelihoods = NULL;
	outputFiles = NULL;
	LL_alt = NULL;
	LL_M0 = NULL;
	weights = NULL;
}

void TPhenotypes::initialize(std::string & PhenoFileName, TVcfFile_base & vcfFile){
	//initialize from VCF file
	numSamples = vcfFile.numSamples();
	sampleNames = new std::string[numSamples];
	for(int i=0; i<numSamples; ++i){
		sampleNames[i] = vcfFile.sampleName(i);
	}
	sampleNamesArrayCreated = true;

	//now read pheno file
	readPhenoFile(PhenoFileName);
}

void TPhenotypes::initialize(std::string & PhenoFileName, TBeagleFile & beagleFile){
	//initialize from BEAGLE file
	numSamples = beagleFile.numSamples();
	sampleNames = beagleFile.getPointerToSampleNames();

	//now read pheno file
	readPhenoFile(PhenoFileName);
}

int TPhenotypes::getSampleNumber(std::string & name){
	for(int i=0; i<numSamples; ++i){
		if(sampleNames[i] == name)
			return i;
	}
	return -1;
}

void TPhenotypes::createStorage(){
	phenotypes = new double*[numPhenotypes];
	for(int i=0; i<numPhenotypes; ++i) phenotypes[i] = new double[numSamples];
	genotypeLikelihoods = new double*[numSamples];
	weights = new double*[numSamples];

	for(int i=0; i<numSamples; ++i){
		genotypeLikelihoods[i] = new double[3];
		weights[i] = new double[3];
	}
	LL_alt = new double[numPhenotypes];
	LL_M0 = new double[numPhenotypes];
	storageCreated = true;
}

void TPhenotypes::readPhenoFile(std::string & PhenoFileName){
	//open file
	phenoFileName = PhenoFileName;
	std::ifstream phenofile(phenoFileName.c_str());
	if(!phenofile) throw "Failed to open file '" + phenoFileName + "'!";

	//read header
	std::vector<std::string> vec;
	fillVectorFromLineWhiteSpaceSkipEmpty(phenofile, vec);
	numPhenotypes = vec.size() - 1;
	phenotypeNames = new std::string[numPhenotypes];
	for(int i=0; i < numPhenotypes; ++i) phenotypeNames[i] = vec[i+1];

	//create storage
	createStorage();
	bool* sampleFound = new bool[numSamples];
	for(int i=0; i<numSamples; ++i)
		sampleFound[i] = false;

	//tmp variables
	long lineNum = 0;
	int sampleNum;

	//parse file and read into vector
	while(phenofile.good() && !phenofile.eof()){
		++lineNum;
		fillVectorFromLineWhiteSpaceSkipEmpty(phenofile, vec);
		//skip empty lines
		if(vec.size() > 0){
			if(vec.size() != (unsigned) numPhenotypes + 1) throw "Wrong number of columns in phenotype file '" + phenoFileName + "' on line " + toString(lineNum) + "!";

			//check if sample exists in VCF / Beagle file
			sampleNum = getSampleNumber(vec[0]);
			if(sampleNum >= 0){
				if(sampleFound[sampleNum]) throw "Sample '" + vec[0] + "' is given multiple times in pheno file '" + phenoFileName + "'!";
				for(int i=0; i<numPhenotypes; ++i){
					phenotypes[i][sampleNum] = stringToDoubleCheck(vec[i+1]);
				}
				sampleFound[sampleNum] = true;
			}
		}
	}

	//check if all samples were found
	for(int i=0; i<numSamples; ++i){
		if(!sampleFound[i]) throw "Phenotype of sample '" + sampleNames[i] + "' is missing in '" + phenoFileName + "'!";
	}

	//now calculate means and variances
	calcMeanAndSdOfPhenotypes();

	//open output files
	openOutputFiles(outputName);

	//clear memory
	delete[] sampleFound;
}

void TPhenotypes::openOutputFiles(std::string & outputName){
	outputFiles = new std::ofstream[numPhenotypes];
	std::string filename, tmp;
	for(int p=0; p<numPhenotypes; ++p){
		tmp = phenotypeNames[p];
		eraseAllWhiteSpaces(tmp);
		filename = outputName + "_associations_" + tmp + ".txt";
		outputFiles[p].open(filename.c_str());
		writeHeader(outputFiles[p]);
	}
	outputFilesOpened = true;
}

void TPhenotypes::writeHeader(std::ofstream & out){
	out << "chr\tpos\tbeta0_null\tsigma2_null\tfAA_null\tfBB_null\tLL_null\tbeta0\tbeta1\tsigma2\tfAA\tfBB\tLL\tD\t-log10(p.val)\n";
}

void TPhenotypes::calcMeanAndSdOfPhenotypes(){
	if(!meansVariancesInitialized){
		means = new double[numPhenotypes];
		variances = new double[numPhenotypes];
		meansVariancesInitialized = true;
	}

	for(int p=0; p<numPhenotypes; ++p){
		//first mean
		means[p] = 0.0;
		for(int i=0; i<numSamples; ++i){
			means[p] += phenotypes[p][i];
		}
		means[p] /= numSamples;

		//now variance
		variances[p] = 0.0;
		for(int i=0; i<numSamples; ++i){
			variances[p] += (means[p] - phenotypes[p][i]) * (means[p] - phenotypes[p][i]);
		}
		if(variances[p] == 0.0) throw "No variance in phenotype '" + phenotypeNames[p] + "'!";
		variances[p] /= numSamples - 1;
	}
}

void TPhenotypes::fillProbGenotypesHW(double & f, double* probGenotypes){
	probGenotypes[0] = (1.0 - f) * (1.0 - f);
	probGenotypes[1] = 2.0 * (1.0 - f) * f;
	probGenotypes[2] = f * f;
}

/*
* This method has been inlined for optimization purpose (in calcLogLikelihood and FindMLEAlternativeModel)
* Thus, it still exists because it is used for the TPhenotypesAdditiveMLEGenotypes::CalcLogLikelihood version
*/
double TPhenotypes::calcProbOfPhenotype(double & phenotype, int & genotype, double* Beta, const double & Sigma2){
	//do not include 1/sqrt(2pi*sigma^2)
	double tmp = phenotype - Beta[0] - Beta[1] * (double) genotype;
	return exp(-0.5 / Sigma2 * tmp * tmp);
}

inline double TPhenotypes::calcLogLikelihood(double* thesePhenotypes, double* Beta, const double & Sigma2, double* & probGenotypes){
	//calc log likelihood at these parameters
	double LL = 0.0;
	double tmp;
	double product_all_tmp = 1.0;
	//int g;
    const double half_over_sigma2 = (-0.5 / Sigma2);
    const double oneOverSqrtTwoPi_over_sqrtSigma2 = oneOverSqrtTwoPi / sqrt(Sigma2);
	for(int i=0; i<numSamples; ++i){
		tmp = 0.0;

		#ifdef WITHOUT_EXP // measurement purpose
			tmp += (half_over_sigma2 * (thesePhenotypes[i] - Beta[0]) * (thesePhenotypes[i] - Beta[0]))
							* genotypeLikelihoods[i][0] * probGenotypes[0]; // g=0
			tmp += (half_over_sigma2 * (thesePhenotypes[i] - Beta[0] - Beta[1]) * (thesePhenotypes[i] - Beta[0] - Beta[1]))
							* genotypeLikelihoods[i][1] * probGenotypes[1]; // g=1
			tmp += (half_over_sigma2 * (thesePhenotypes[i] - Beta[0] - 2.0 * Beta[1]) * (thesePhenotypes[i] - Beta[0] - 2*Beta[1]))
							* genotypeLikelihoods[i][2] * probGenotypes[2]; // g=2
		#else
			tmp += fmath::expd(half_over_sigma2 * (thesePhenotypes[i] - Beta[0]) * (thesePhenotypes[i] - Beta[0]))
							* genotypeLikelihoods[i][0] * probGenotypes[0]; // g=0
			tmp += fmath::expd(half_over_sigma2 * (thesePhenotypes[i] - Beta[0] - Beta[1]) * (thesePhenotypes[i] - Beta[0] - Beta[1]))
							* genotypeLikelihoods[i][1] * probGenotypes[1]; // g=1
			tmp += fmath::expd(half_over_sigma2 * (thesePhenotypes[i] - Beta[0] - 2.0 * Beta[1]) * (thesePhenotypes[i] - Beta[0] - 2*Beta[1]))
							* genotypeLikelihoods[i][2] * probGenotypes[2]; // g=2
		#endif

		//product_all_tmp *= tmp;
		#ifdef WITHOUT_LOG // measurement purpose
			LL += (oneOverSqrtTwoPi_over_sqrtSigma2 * tmp);
		#else
			// notice that fmath::log return a float precision result
			LL += fmath::log(oneOverSqrtTwoPi_over_sqrtSigma2 * tmp);
			// the line below uses the standard library, using double precision
			//LL += log(oneOverSqrtTwoPi_over_sqrtSigma2 * tmp);
		#endif
	}
	// LL = log(oneOverSqrtTwoPi_over_sqrtSigma2) * numSamples + log(product_all_tmp); // + sum log ( tmp )
	// doesn't work perfectly, should reduce the number of log groupped together (by batch smaller than numSamples)
	return LL;
}

bool TPhenotypes::findMLENullModel(double & freqFilter){
	//beta0 and epsilon are mean and variance of phenotypes -> already calculated
	//so we just need to estimate genotype frequencies -> use EM
	//Note: in this model, genotype frequencies are the same for for all phenotypes!
	estimateGenotypeFrequenciesNullModel(genotypeFrequencies_M0, numSamples, genotypeLikelihoods, epsilonF);

	//check if frequencies pass filter
	double f = (0.5 * genotypeFrequencies_M0[1] + genotypeFrequencies_M0[2]);
	if(f < freqFilter || (1.0-f) < freqFilter) return false;

	//calculate likelihood and write output
	double beta[3];
	beta[1] = 0.0;
	beta[2] = 0.0;
	for(int p=0; p<numPhenotypes; ++p){
		beta[0] = means[p];
		LL_M0[p] = calcLogLikelihood(phenotypes[p], beta, variances[p], genotypeFrequencies_M0);
		outputFiles[p] << chrPos << "\t" << means[p] << "\t" << variances[p] << "\t" << genotypeFrequencies_M0[0] << "\t" << genotypeFrequencies_M0[2] << "\t" << LL_M0[p];
	}
	return true;
}

void TPhenotypes::findMLEAlternativeModel(){
	//tmp variables
	int i, g;
	double tmp;
	double Sum_w, Sum_wg, Sum_wg2, Sum_wy, Sum_wgy, Sum_sigma;
	double D;
	double beta[2];
	double beta_old[2];
	double sigma2;
	double genoFreq_old[3];
	double sigma2_old, LL_old;

	//This is done using an EM algorithm, separately for each phenotype
	for(int p=0; p<numPhenotypes; ++p){
		//start @ Null model
		// -> it is assumed that the Null model has already been estimated!
		beta[0] = means[p];
		beta[1] = 0.0;
		sigma2 = variances[p];

		genotypeFrequencies_alt[0] = genotypeFrequencies_M0[0];
		genotypeFrequencies_alt[1] = genotypeFrequencies_M0[1];
		genotypeFrequencies_alt[2] = genotypeFrequencies_M0[2];
		
		LL_alt[p] = LL_M0[p];


		#ifdef MEASURE_STEP_ITERATION
			clock_serv_t cclock;
			mach_timespec_t start, finish;
			host_get_clock_service(mach_host_self(), CALENDAR_CLOCK, &cclock);
			// measure time at the beginnig of the iteration (try max 1000 attemps loop)
			clock_get_time(cclock, &start);
		#endif
		//run EM for max 1000 steps
		for(int s=0; s<1000; ++s){
			//save old params
			beta_old[0] = beta[0];
			beta_old[1] = beta[1];
			sigma2_old = sigma2;
			const double half_over_sigma2_old = -0.5 / sigma2_old;
			LL_old = LL_alt[p];

			genoFreq_old[0] = genotypeFrequencies_alt[0];
			genoFreq_old[1] = genotypeFrequencies_alt[1];
			genoFreq_old[2] = genotypeFrequencies_alt[2];
			genotypeFrequencies_alt[0] = genotypeFrequencies_alt[1] = genotypeFrequencies_alt[2] = 0.0;

			//set sums = 0
			Sum_w = Sum_wg = Sum_wg2 = Sum_wy = Sum_wgy = 0.0;
			
			//calculate weights, add to sums and estimate new f
			for(i=0; i<numSamples; ++i){
				const double phenotype_p_i = phenotypes[p][i];
				double* const weights_i = weights[i];
				double tmpProb = phenotype_p_i - beta_old[0];
				
				#ifdef WITHOUT_EXP // measurement purpose
					weights_i[0] = (half_over_sigma2_old * tmpProb * tmpProb) * genotypeLikelihoods[i][0] * genoFreq_old[0];
					tmpProb -= beta_old[1];
					weights_i[1] = (half_over_sigma2_old * tmpProb * tmpProb) * genotypeLikelihoods[i][1] * genoFreq_old[1];
					tmpProb -= beta_old[1];
					weights_i[2] = (half_over_sigma2_old * tmpProb * tmpProb) * genotypeLikelihoods[i][2] * genoFreq_old[2];
				#else
					weights_i[0] = fmath::expd(half_over_sigma2_old * tmpProb * tmpProb) * genotypeLikelihoods[i][0] * genoFreq_old[0];
					tmpProb -= beta_old[1];
					weights_i[1] =  fmath::expd(half_over_sigma2_old * tmpProb * tmpProb) * genotypeLikelihoods[i][1] * genoFreq_old[1];
					tmpProb -= beta_old[1];
					weights_i[2] = fmath::expd(half_over_sigma2_old * tmpProb * tmpProb) * genotypeLikelihoods[i][2] * genoFreq_old[2];
				#endif

				tmp = weights_i[0] + weights_i[1] + weights_i[2];

				weights_i[0] /= tmp;
				weights_i[1] /= tmp;
				weights_i[2] /= tmp;
				Sum_wgy += 	phenotype_p_i * (weights_i[1] + weights_i[2] * 2.0);
				Sum_wy 	+= 	phenotype_p_i * (weights_i[0] + weights_i[1] + weights_i[2]);
				Sum_wg2 += 	weights_i[1] + weights_i[2] * 4.0;
				Sum_wg 	+=	weights_i[1] + weights_i[2] * 2.0;
				Sum_w 	+= 	weights_i[0] + weights_i[1] + weights_i[2];

				//add to estimate genotype frequencies
				genotypeFrequencies_alt[0] += weights_i[0];
				genotypeFrequencies_alt[2] += weights_i[2];

			}

			//estimate new genotype frequencies
			genotypeFrequencies_alt[0] /= (double) numSamples;
			genotypeFrequencies_alt[2] /= (double) numSamples;
			genotypeFrequencies_alt[1] = 1.0 - genotypeFrequencies_alt[0] - genotypeFrequencies_alt[2];

			//estimate new beta
			tmp = Sum_w * Sum_wg2 - Sum_wg * Sum_wg;
			beta[0] = (Sum_wg2 * Sum_wy - Sum_wg * Sum_wgy) / tmp;
			beta[1] = (Sum_w * Sum_wgy - Sum_wg * Sum_wy) / tmp;

			//now estimate new sigma
			Sum_sigma = 0.0;
			for(i=0; i<numSamples; ++i){
				tmp = phenotypes[p][i] - beta[0];
				Sum_sigma +=  weights[i][0] * tmp * tmp;
				tmp -= beta[1];
				Sum_sigma +=  weights[i][1] * tmp * tmp;
				tmp -= beta[1];
				Sum_sigma +=  weights[i][2] * tmp * tmp;

				/* 	another approach (no relevant benefit)
					perform more computation, but write the 'Sum_sigma' variable only once
						Sum_sigma 	+=  weights[i][0] * (phenotypes[p][i] - beta[0]) * (phenotypes[p][i] - beta[0])
									+	weights[i][1] * (phenotypes[p][i] - beta[0] - beta[1]) * (phenotypes[p][i] - beta[0] - beta[1])
									+	weights[i][2] * (phenotypes[p][i] - beta[0] - beta[1] - beta[1]) * (phenotypes[p][i] - beta[0] - beta[1] - beta[1]);
				*/
			}
			sigma2 = Sum_sigma / Sum_w;

			//calc LL to check for convergence
			LL_alt[p] = calcLogLikelihood(phenotypes[p], beta, sigma2, genotypeFrequencies_alt);

			#ifdef MEASURE_STEP_ITERATION // measure the number of iteration of the 'try 1000 steps' loop
				nb_attempt_iteration++;
			#endif

			//test if convergence was reached
			if(LL_alt[p] - LL_old < epsilonLL) break;
		}
		#ifdef MEASURE_STEP_ITERATION // add the current iteration time to the total time (i.e. measure loop time)
			clock_get_time(cclock, &finish);
			time_for_all_step_iterations += (finish.tv_sec - start.tv_sec) + (finish.tv_nsec - start.tv_nsec) / 1.0e9; // time sec
		#endif

		//print params
		outputFiles[p] << "\t" << beta[0] << "\t" << beta[1] << "\t" << sigma2 << "\t" << genotypeFrequencies_alt[0] <<"\t" << genotypeFrequencies_alt[2] << "\t" << LL_alt[p];

		//now perform Log-Ratio Test to get p-value and print
		D = 2.0 * (LL_alt[p] - LL_M0[p]);
		writeLog10PValue(D, 1, outputFiles[p]);
		outputFiles[p] << "\n";
	}

	#ifdef MEASURE_STEP_ITERATION
		printf("time_for_all_step_iterations : %f\tnb_attempt_iteration : %ld\n", time_for_all_step_iterations, nb_attempt_iteration);
	#endif
}

void TPhenotypes::writeLog10PValue(double & D, int DF, std::ofstream & out){
	//write D
	out << "\t" << D << "\t";

	//calc p value
	double* thisCisqDist;
	if(DF == 1) thisCisqDist = chisq_1DF;
	else if(DF == 2) thisCisqDist = chisq_2DF;
	else throw "Chi-square distribution for DF = " + toString(DF) + " is not implemented!";
	if(D > thisCisqDist[144]){
		out << "16.0";
	} else {
		int x = 0;
		while(thisCisqDist[x] < D) ++x;
		double tmp = chisqPval[x-1] + (D - thisCisqDist[x-1])/(thisCisqDist[x]-thisCisqDist[x-1])*(chisqPval[x]-chisqPval[x-1]);
		out << -log10(tmp);
	}
}

bool TPhenotypes::performAssociationTest_core(double & freqFilter){
	//first find MLE of null model
	if(!findMLENullModel(freqFilter)) return false;
		// if position has enough data

	//now find MLE for alternative model
	findMLEAlternativeModel();

	return true;
}

bool TPhenotypes::performAssociationTest(TVcfFileSingleLine & vcfFile, double & freqFilter){
	//read genotype likelihoods of first two alleles from file
	for(int i=0; i<numSamples; ++i){
		vcfFile.fillGenotypeLikelihoods(i, genotypeLikelihoods[i]);
	}
	chrPos = vcfFile.chr() + "\t" + toString(vcfFile.position());
	return performAssociationTest_core(freqFilter);
}

bool TPhenotypes::performAssociationTest(TBeagleFile & beagleFile, double & freqFilter){
	//read genotype likelihoods of first two alleles from file
	beagleFile.fillGenotypeLikelihoods(genotypeLikelihoods);

	chrPos = beagleFile.getChrPosString();
	return performAssociationTest_core(freqFilter);
}

//--------------------------------------
//TPhenotypesAdditiveMLEGenotypes
//--------------------------------------
void TPhenotypesAdditiveMLEGenotypes::createStorage(){
	TPhenotypes::createStorage();
	MLEGenotypes = new int[numSamples];
	genotypeCalled = new bool[numSamples];
}

double TPhenotypesAdditiveMLEGenotypes::calcLogLikelihood(double* thesePhenotypes, double* Beta, double & Sigma2){
	//calc log likelihood at these parameters
	double LL = 0.0;
	for(int i=0; i<numSamples; ++i){
		LL += log(oneOverSqrtTwoPi / sqrt(Sigma2) * calcProbOfPhenotype(thesePhenotypes[i], MLEGenotypes[i], Beta, Sigma2));
	}
	return LL;
}

double TPhenotypesAdditiveMLEGenotypes::getMLEGenotypesAndCalcAlleleFrequency(){
	double f = 0.0;
	numSamplesWithGenotype = 0;
	for(int i=0; i<numSamples; ++i){
		//do we have data?
		//TODO: figure out how to check that. Use quality filter?
		if(1==0){
			genotypeCalled[i] = false;
		} else {
			//yes, there is data
			++numSamplesWithGenotype;
			genotypeCalled[i] = true;
			if(genotypeLikelihoods[i][1] > genotypeLikelihoods[i][0]){
				if(genotypeLikelihoods[i][2] > genotypeLikelihoods[i][1]){
					MLEGenotypes[i] = 2;
					genotypeFrequencies_M0[2] += 1.0;
					f += 1.0;
				} else {
					MLEGenotypes[i] = 1;
					genotypeFrequencies_M0[1] += 1.0;
					f += 0.5;
				}
			} else {
				if(genotypeLikelihoods[i][2] > genotypeLikelihoods[i][0]){
					MLEGenotypes[i] = 2;
					genotypeFrequencies_M0[2] += 1.0;
					f += 1.0;
				} else {
					genotypeFrequencies_M0[0] += 1.0;
					MLEGenotypes[i] = 0;
				}
			}
		}
	}
	for(int g=0; g<3; ++g)
		genotypeFrequencies_M0[g] /= (double) numSamplesWithGenotype;
	return f / (double) numSamplesWithGenotype;
}

bool TPhenotypesAdditiveMLEGenotypes::findMLENullModel(double & freqFilter){
	//beta0 and epsilon are mean and variance of phenotypes -> already calculated
	double f = getMLEGenotypesAndCalcAlleleFrequency();

	//check if frequencies pass filter
	if(f < freqFilter || (1.0-f) < freqFilter) return false;

	//calculate likelihood and write output
	double beta[3];
	beta[1] = 0.0;
	beta[2] = 0.0;
	for(int p=0; p<numPhenotypes; ++p){
		beta[0] = means[p];
		LL_M0[p] = calcLogLikelihood(phenotypes[p], beta, variances[p]);
		outputFiles[p] << chrPos << "\t" << means[p] << "\t" << variances[p] << "\t" << genotypeFrequencies_M0[0] << "\t" << genotypeFrequencies_M0[2] << "\t" << LL_M0[p];
	}
	return true;
}

void TPhenotypesAdditiveMLEGenotypes::findMLEAlternativeModel(){
	//estimate beta_0 and beta_1 using OLS

	double sum_x = 0.0;
	double sum_y = 0.0;
	double sum_xx = 0.0;
	double sum_xy = 0.0;
	double beta[2];
	double sigma2, tmp, D;

	//first sum of x and xx: are the same for all phenotypes
	for(int i=0; i<numSamples; ++i){
		if(genotypeCalled){
			sum_x += MLEGenotypes[i];
			sum_xx += MLEGenotypes[i] * MLEGenotypes[i];
		}
	}

	//now rest for each phenotype
	for(int p=0; p<numPhenotypes; ++p){
		//build sums of y and xy
		sum_y = 0.0;
		sum_xy = 0.0;
		for(int i=0; i<numSamples; ++i){
			if(genotypeCalled){
				sum_y += phenotypes[p][i];
				sum_xy += MLEGenotypes[i] * phenotypes[p][i];
			}
		}

		//estimate beta0 and beta1
		tmp = numSamplesWithGenotype * sum_xx - sum_x * sum_x;
		beta[0] = (sum_xx * sum_y - sum_x * sum_xy) / tmp;
		beta[1] = (numSamplesWithGenotype * sum_xy - sum_x * sum_y) / tmp;

		//estimate sigma2 using beta0 and beta1
		sigma2 = 0.0;
		for(int i=0; i<numSamples; ++i){
			if(genotypeCalled){
				tmp = phenotypes[p][i] - beta[0] - beta[1] * MLEGenotypes[i];
				sigma2 += tmp * tmp;
			}
		}
		sigma2 /= numSamplesWithGenotype - 1;

		//calc LL
		LL_alt[p] = calcLogLikelihood(phenotypes[p], beta, sigma2);

		//print params
		outputFiles[p] << "\t" << beta[0] << "\t" << beta[1] << "\t" << sigma2 << "\t" << genotypeFrequencies_M0[0] <<"\t" << genotypeFrequencies_M0[2] << "\t" << LL_alt[p];

		//now perform Log-Ratio Test to get p-value and print
		D = 2.0 * (LL_alt[p] - LL_M0[p]);
		writeLog10PValue(D, 1, outputFiles[p]);
		outputFiles[p] << "\n";
	}
};

//--------------------------------------
//TPhenotypesGeneralized
//--------------------------------------
void TPhenotypesGeneralized::writeHeader(std::ofstream & out){
	out << "chr\tpos\tbeta0_null\terrorVar_null\tf_null\tLL_null\tbeta0\tbeta1\tbeta2\terrorVar\tf\tLL\tD\tlog10(p.val)\n";
}

double TPhenotypesGeneralized::calcProbOfPhenotype(double & phenotype, int & genotype, double* Beta, double & Sigma2){
	//do not include 1/sqrt(2pi*sigma^2)
	double tmp = phenotype - Beta[genotype];
	return exp(-0.5 / Sigma2 * tmp * tmp);
}

void TPhenotypesGeneralized::findMLEAlternativeModel(){
	//tmp variables
	int i, g;
	double tmp;
	double Sum_w[3];
	double Sum_wy[3];
	double Sum_sigma;
	double D;
	double beta[3];
	double sigma2;
	double beta_old[3];
	double genoFreq_old[3];
	double sigma_old, LL_old;

	//This is done using an EM algorithm, separately for each phenotype
	for(int p=0; p<numPhenotypes; ++p){
		//start @ Null model
		// -> it is assumed that the Null model has already been estimated!
		beta[0] = means[p];
		beta[1] = means[p];
		beta[2] = means[p];
		sigma2 = variances[p];
		for(g=0; g<3; ++g)
			genotypeFrequencies_alt[g] = genotypeFrequencies_M0[g];
		LL_alt[p] = LL_M0[p];

		//run EM for max 1000 steps
		for(int s=0; s<1000; ++s){
			//save old params
			beta_old[0] = beta[0];
			beta_old[1] = beta[1];
			beta_old[2] = beta[2];
			sigma_old = sigma2;
			LL_old = LL_alt[p];
			for(g=0; g<3; ++g){
				genoFreq_old[g] = genotypeFrequencies_alt[g];
				genotypeFrequencies_alt[g] = 0.0;
			}


			//set sums = 0
			/*
			for(g=0; g<3; ++g){
				Sum_w[g] = 0.0;
				Sum_wy[g] = 0.0;
			}
			*/
			Sum_w[0] = Sum_w[1] = Sum_w[2] = Sum_wy[0] = Sum_wy[1] = Sum_wy[2] = 0.0;

			//calculate weights, add to sums and estimate new f
			for(i=0; i<numSamples; ++i){
				//calculate weights
				tmp = 0.0;
				for(g=0; g<3; ++g){
					// weights[i][g] = calcProbOfPhenotype(phenotypes[p][i], g, beta_old, sigma_old) * genotypeLikelihoods[i][g] * genoFreq_old[g];
					tmp += weights[i][g];
				}

				//normalize and add to sums
				for(g=0; g<3; ++g){
					weights[i][g] /= tmp;
					Sum_w[g] += weights[i][g];
					Sum_wy[g] += weights[i][g] * phenotypes[p][i];
				}

				//add to estimate genotype frequencies
				genotypeFrequencies_alt[0] += weights[i][0];
				genotypeFrequencies_alt[2] += weights[i][2];

			}

			//estimate new genotype frequencies
			genotypeFrequencies_alt[0] /= (double) numSamples;
			genotypeFrequencies_alt[2] /= (double) numSamples;
			genotypeFrequencies_alt[1] = 1.0 - genotypeFrequencies_alt[0] - genotypeFrequencies_alt[2];

			//estimate new beta
			for(g=0; g<3; ++g){
				beta[g] = Sum_wy[g] / Sum_w[g];
			}

			//now estimate new sigma
			Sum_sigma = 0.0;
			for(i=0; i<numSamples; ++i){
				for(g=0; g<3; ++g){
					tmp = phenotypes[p][i] - beta[g];
					Sum_sigma +=  weights[i][g] * tmp * tmp;
				}
			}
			sigma2 = Sum_sigma / (Sum_w[0] + Sum_w[1] + Sum_w[2]);

			//calc LL to check for convergence
			LL_alt[p] = calcLogLikelihood(phenotypes[p], beta, sigma2, genotypeFrequencies_alt);

			//test if convergence was reached
			if(LL_alt[p] - LL_old < epsilonLL) break;
		}

		//print params
		outputFiles[p] << "\t" << beta[0] << "\t" << beta[1] << "\t" << beta[2] << "\t" << sigma2 << "\t" << genotypeFrequencies_alt[0] << "\t" << genotypeFrequencies_alt[2] << "\t" << LL_alt[p];

		//now perform Log-Ratio Test to get p-value and print
		D = 2.0 * (LL_alt[p] - LL_M0[p]);
		writeLog10PValue(D, 1, outputFiles[p]);
		outputFiles[p] << "\n";
	}
}

/*
//--------------------------------------
//TPhenotypesLogit
//--------------------------------------
TPhenotypesLogit::TPhenotypesLogit(std::string & OutputName):TPhenotypes(OutputName){
	numParams = 2;
	epsilonNR = 0.0001;

	//set all x_g that are not 0.0
	createParamStorage();
	x_g[0][0] = 1.0;
	x_g[1][0] = 1.0;
	x_g[1][1] = 1.0;
	x_g[2][0] = 1.0;
	x_g[2][1] = 2.0;
};

void TPhenotypesLogit::createParamStorage(){
	F = new double[numParams];
	x_g = new double*[3];
	for(int g=0; g<3; ++g){
		x_g[g] = new double[numParams];
		for(int j=0; j<numParams; ++j){
			x_g[g][j] = 0.0;
		}
	}

	J = new double*[numParams];
	J_inv = new double*[numParams];
	for(int j=0; j<numParams; ++j){
		J[j] = new double[numParams];
		J_inv[j] = new double[numParams];
	}

	beta = new double[numParams];
	beta_old = new double[numParams];
}


void TPhenotypesLogit::createStorage(){
	TPhenotypes::createStorage();
	beta_null = new double[numPhenotypes];
}

void TPhenotypesLogit::checkForBinaryPhenotypes(){
	for(int p=0; p<numPhenotypes; ++p){
		for(int i=0; i<numSamples; ++i){
			if(phenotypes[p][i] < -0.01 || phenotypes[p][i] > 1.01) throw "Phenotype " + toString(p+1) + " is not binary!";
			if(phenotypes[p][i] > 0.01 && phenotypes[p][i] < 0.99)  throw "Phenotype " + toString(p+1) + " is not binary!";
		}
	}
}

void TPhenotypesLogit::initialize(std::string & PhenoFileName, TVcfFile_base & vcfFile){
	TPhenotypes::initialize(PhenoFileName, vcfFile);
	checkForBinaryPhenotypes();
}

void TPhenotypesLogit::initialize(std::string & PhenoFileName, TBeagleFile & beagleFile){
	TPhenotypes::initialize(PhenoFileName, beagleFile);
	checkForBinaryPhenotypes();
}


void TPhenotypesLogit::writeHeader(std::ofstream & out){
	out << "chr\tpos\tbeta0_null\tf_null\tLL_null\tbeta0\tbeta1\tf\tLL\tD\tlog10(p.val)\n";
}

double TPhenotypesLogit::logit(double z){
	return 1.0 / (1.0 + exp(-z));
}

double TPhenotypesLogit::calcProbOfPhenotypeLogit(int genotype, double* Beta){
	double tmp = Beta[0]*x_g[genotype][0] + Beta[1]*x_g[genotype][1];
	return logit(tmp);
}

double TPhenotypesLogit::calcLogLikelihoodLogit(double* thesePhenotypes, double* Beta, double* & probGenotypes){
	//calc log likelihood at these parameters
	double LL = 0.0;
	double tmp;
	int g;
	for(int i=0; i<numSamples; ++i){
		tmp = 0.0;
		if(thesePhenotypes[i] > 0.99){
			for(g=0; g<3; ++g){
				tmp += calcProbOfPhenotypeLogit(g, Beta) * genotypeLikelihoods[i][g] * probGenotypes[g];
			}
		} else {
			for(g=0; g<3; ++g){
				tmp += (1.0 - calcProbOfPhenotypeLogit(g, Beta)) * genotypeLikelihoods[i][g] * probGenotypes[g];
			}
		}
		LL += log(tmp);
	}
	return LL;
}

void TPhenotypesLogit::findMLENullModel(double & initialF){
	//Note: in this model, f is the same for all phenotypes!
	estimateFNullModel(initialF);

	//estimate beta0
	int numPhenoOne;
	double tmp;
	double beta_tmp[2];
	beta_tmp[1] = 0.0;
	for(int p=0; p<numPhenotypes; ++p){
		//count how many have phenotype 1
		numPhenoOne = 0;
		for(int i=0; i<numSamples; ++i)
			if(phenotypes[p][i] > 0.99) ++numPhenoOne;
		tmp = (double) numPhenoOne / (double) numSamples;
		beta_null[p] = log(tmp) - log(1.0 - tmp);

		//calculate likelihood and write output
		beta_tmp[0] = beta_null[p];
		LL_M0[p] = calcLogLikelihoodLogit(phenotypes[p], beta_tmp, probGenotypes);
		outputFiles[p] << chrPos << "\t" << beta_null[p] << "\t" << f_M0 << "\t" << LL_M0[p];
	}
}

void TPhenotypesLogit::inverseJ(){
	double det = (J[0][0] * J[1][1] - J[0][1] * J[1][0]);
	J_inv[0][0] = J[1][1] / det;
	J_inv[0][1] = -J[0][1] / det;
	J_inv[1][0] = -J[1][0] / det;
	J_inv[1][1] = J[0][0] / det;
}

double TPhenotypesLogit::calculateWeightsAndEstimateFAlternativeModel(int & phenotype){
	double sum;
	int g;
	double logitPheno;
	double Sum_f = 0;

	for(int i=0; i<numSamples; ++i){
		sum = 0.0;
		for(g=0; g<3; ++g){
			logitPheno = calcProbOfPhenotypeLogit(g, beta_old);
			if(phenotypes[phenotype][i] > 0.99) weights[i][g] = logitPheno * genotypeLikelihoods[i][g] * probGenotypes[g];
			else weights[i][g] = (1.0 - logitPheno) * genotypeLikelihoods[i][g] * probGenotypes[g];
			sum += weights[i][g];
		}

		//normalize and add to sums
		for(g=0; g<3; ++g)
			weights[i][g] /= sum;

		//add to sums for f
		Sum_f += 2.0 * weights[i][2] + weights[i][1];
	}
	return Sum_f / ( 2.0 * (double) numSamples);
}


void TPhenotypesLogit::runNewtonRaphson(int & phenotype){
	//run Newton-Raphson to estimate all beta
	//tmp variables
	int i, j, k, g;
	double tmp2;
	double logitPheno;
	double maxF;

	//std::cout << "x)\t" << beta[0] << "\t" << beta[1] << "\t" << std::endl;

	//run for may 1000 iterations
	for(int n=0; n<1000; ++n){
		//set J and F to zero
		for(j=0; j<numParams; ++j){
			F[j] = 0.0;
			for(int k=0; k<numParams; ++k){
				J[j][k] = 0.0;
			}
		}

		//calculate weights, add to sums of J and F
		for(i=0; i<numSamples; ++i){
			//add to sums
			for(g=0; g<3; ++g){
				logitPheno = calcProbOfPhenotypeLogit(g, beta);
				//add to F
				tmp2 = weights[i][g] * (phenotypes[phenotype][i] - logitPheno);
				for(j=0; j<numParams; ++j)
					F[j] += tmp2 * x_g[g][j];

				//add to J
				tmp2 = weights[i][g] * logitPheno * (1.0 - logitPheno);
				for(j=0; j<numParams; ++j){
					for(k=0; k<numParams; ++k)
						J[j][k] -= tmp2 * x_g[g][j] * x_g[g][k];
				}
			}
		}

		//now estimate new parameters
		inverseJ();
		for(j=0; j<numParams; ++j){
			for(k=0; k<numParams; ++k){
				beta[j] -= J_inv[j][k] * F[k];
			}
		}

		//std::cout << n << ")\t" << beta[0] << "\t" << beta[1] << "\t" << std::endl;


		//did Newton-Raphson converge? -> get largest F
		maxF = 0.0;
		for(j=0; j<numParams; ++j)
			if(fabs(F[j]) > maxF) maxF = fabs(F[j]);
		if(maxF < epsilonNR) break;
	}
}

void TPhenotypesLogit::findMLEAlternativeModel(){
	//tmp variables
	double D;
	double f;
	double LL_old;

	//This is done using an EM algorithm, separately for each phenotype
	for(int p=0; p<numPhenotypes; ++p){
		//start @ Null model
		// -> it is assumed that the Null model has already been estimated!
		beta[0] = beta_null[p];
		beta[1] = 0.0;
		f = f_M0;
		LL_alt[p] = LL_M0[p];

		//calc initial P(g|f)
		fillProbGenotypesHW(f, probGenotypes);

		//run EM for max 1000 steps
		for(int s=0; s<1000; ++s){
			//save old params
			for(int j=0; j<numParams; ++j)
				beta_old[j] = beta[j];
			LL_old = LL_alt[p];

			//calc weights and estimate new f
			f = calculateWeightsAndEstimateFAlternativeModel(p);

			//run Newton Raphson to estimate all beta
			runNewtonRaphson(p);

			//calc new P(g|f) for next iteration and to calc LL
			fillProbGenotypesHW(f, probGenotypes);

			//calc LL to check for convergence
			LL_alt[p] = calcLogLikelihoodLogit(phenotypes[p], beta, probGenotypes);


			//std::cout << s << ")\t" << beta[0] << "\t" << beta[1] << "\t" << f << "\t" << LL_alt[p] << std::endl;

			//test if convergence was reached
			//if(LL_alt[p] - LL_old < epsilonLL) break;
		}

		//print params
		for(int j=0; j<numParams; ++j)
			outputFiles[p] << "\t" << beta[j];
		outputFiles[p] << "\t" << f << "\t" << LL_alt[p];

		//now perform Log-Ratio Test to get p-value and print
		D = 2.0 * (LL_alt[p] - LL_M0[p]);
		writeLog10PValue(D, numParams - 1, outputFiles[p]);
		outputFiles[p] << "\n";
	}
}

*/




/*
 * TLinkageModel.h
 *
 *  Created on: Nov 1, 2016
 *      Author: wegmannd
 */

#ifndef TLINKAGEMODEL_H_
#define TLINKAGEMODEL_H_

#include "TVcfFile.h"
#include "TBeagleFile.h"
#include "TLog.h"
#include "TParameters.h"
#include "commonFunctions.h"
#include <iostream>
#include <fstream>
#include "gzstream.h"

class TLinkageModelEstimator{
private:
	int numInd;
	int windowSize;
	int overlap;
	int halfOverlap;
	int lastSiteWithDataPlusOne;
	int blockSize_K;
	int numStates_B;
	int twoPowKMinusOne;
	int numStates_BB;
	TLog* logfile;
	double freqFilter;
	double*** genotypeLikelihoods; //per individual, locus, genotype
	double*** alpha; //per locus and state1, state2
	double*** beta; //per cur or next, state1, stat2
	double*** gamma; //locus, stat1 and state2
	double*** xi; //state1, state2 and follower state (00->0, 01->1, 10->2, 11->3)
	double*** zeta; //state1, state2 and follower state (00->0, 01->1, 10->2, 11->3)
	double***** zetaProd; //per cur or next, state1 & state2 @ l, state1 & state2 @ l+1
	double*** r; //per locus, state, r or 1-r
	double** r_new; //per locus and state
	double** r_new_denominator; //per locus and state

	int** nextState;
	int** previousState;

	TBeagleFile* beagleFile;
	TVcfFile_base* vcfFile;
	bool isBeagle, isVCF;
	std::string* locusName;

	int maxNumBWIterations;
	double maxDeltaLL;
	double deltaF;

public:
	TLinkageModelEstimator(TParameters & params, TLog* Logfile, TBeagleFile* BeagleFile);
	TLinkageModelEstimator(TParameters & params, TLog* Logfile, TVcfFile_base* VcfFile);
	~TLinkageModelEstimator();
	void initialize(TParameters & params, TLog* Logfile);
	void readData();
	double runForwardBackward();
	void estimate(std::string & outputName);
	void writeCurrentEstimatesToFile(gz::ogzstream & outfile);


	void calcPairwiseCorrelations(std::string & outputName);



};


#endif /* TLINKAGEMODEL_H_ */

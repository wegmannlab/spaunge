/*
 * commonFunctions.cpp
 *
 *  Created on: Nov 4, 2016
 *      Author: wegmannd
 */

#include "commonFunctions.h"

void fillInitialEstimateOfGenotypeFrequencies(double* genoFreq, int & numSamples, double** genotypeLikelihoods){
	//calculate by using MLE genotype for each individual
	genoFreq[0] = 0.0; genoFreq[1] = 0.0; genoFreq[2] = 0.0;
	for(int i=0; i<numSamples; ++i){
		if(genotypeLikelihoods[i][1] > genotypeLikelihoods[i][0]){
			if(genotypeLikelihoods[i][2] > genotypeLikelihoods[i][1]) genoFreq[2] += 1.0;
			else genoFreq[1] += 1.0;
		} else {
			if(genotypeLikelihoods[i][2] > genotypeLikelihoods[i][0]) genoFreq[2] += 1.0;
			else genoFreq[0] += 1.0;
		}
		/*
		for(int g = 0; g<3; ++g){
			genoFreq[g] += genotypeLikelihoods[i][g];
		}
		*/
	}

	double sum = 0.0;
	for(int g = 0; g<3; ++g){
		genoFreq[g] /= (double) numSamples;
		if(genoFreq[g] <= 0.0) genoFreq[g] = 0.01;
		if(genoFreq[g] >= 1.0) genoFreq[g] = 0.99;
		sum += genoFreq[g];
	}
	for(int g = 0; g<3; ++g){
		genoFreq[g] /= sum;
	}



}

void estimateGenotypeFrequenciesNullModel(double* genotypeFrequencies, int & numSamples, double** genotypeLikelihoods, double epsilonF){
	//prepare variables
	double sum;
	int i, g;
	double weightsNull[3];
	double genoFreq_old[3];

	//estimate initial frequencies from MLEs
	fillInitialEstimateOfGenotypeFrequencies(genotypeFrequencies, numSamples, genotypeLikelihoods);

	//run EM for max 1000 steps
	for(int s=0; s<1000; ++s){
		//set genofreq and calc P(g|f)
		for(g=0; g<3; ++g){
			genoFreq_old[g] = genotypeFrequencies[g];
			genotypeFrequencies[g] = 0.0;
		}

		//estimate new genotye frequencies
		for(i=0; i<numSamples; ++i){
			sum = 0.0;
			for(g=0; g<3; ++g){
				weightsNull[g] = genotypeLikelihoods[i][g] * genoFreq_old[g];
				sum += weightsNull[g];
			}
			genotypeFrequencies[0] += weightsNull[0] / sum;
			genotypeFrequencies[2] += weightsNull[2] / sum;
		}

		genotypeFrequencies[0] /= (double) numSamples;
		genotypeFrequencies[2] /= (double) numSamples;
		genotypeFrequencies[1] = 1.0 - genotypeFrequencies[0] - genotypeFrequencies[2];

		//check if we stop
		if(fabs(genotypeFrequencies[0] - genoFreq_old[0]) < epsilonF && fabs(genotypeFrequencies[2] - genoFreq_old[2]) < epsilonF) break;
	}
}





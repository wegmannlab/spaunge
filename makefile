#make file for spaunge

CC=g++

SRC = $(wildcard *.cpp) 

OBJ = $(SRC:%.cpp=%.o)

BIN = spaunge

all:	$(BIN)

$(BIN):	$(OBJ)
	$(CC) -O3 -o $(BIN) $(OBJ) -lz

%.o: %.cpp
	$(CC) -O3 -c -std=c++1y $< -o $@

clean:
	rm -rf *.o spaunge




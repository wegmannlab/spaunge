/*
 * TPhenotypeStorage.h
 *
 *  Created on: Feb 19, 2016
 *      Author: wegmannd
 */

#ifndef TPHENOTYPES_H_
#define TPHENOTYPES_H_

#include <vector>
#include "TVcfFile.h"
#include "TBeagleFile.h"
#include "commonFunctions.h"

//--------------------------------------
//TPhenotypes
//--------------------------------------
class TPhenotypes{
protected:
	std::string phenoFileName;
	std::string outputName;
	int numPhenotypes;
	int numSamples;
	std::string* phenotypeNames;
	double** phenotypes;

	std::string* sampleNames;
	bool sampleNamesArrayCreated;
	double** genotypeLikelihoods;
	double* means;
	double* variances;
	bool meansVariancesInitialized;
	bool storageCreated;

	//output files
	bool outputFilesOpened;
	std::ofstream* outputFiles;

	//parameters for model and EM
	double* LL_M0;
	double* LL_alt;
	double* genotypeFrequencies_M0;
	double* genotypeFrequencies_alt;

	//tmp parameters during EM
	double** weights;
	std::string chrPos;

	//convergence params
	double epsilonLL;
	double epsilonF;

	//other things
	double oneOverSqrtTwoPi;
	double chisq_1DF[145] = {0.00000000, 0.01579077, 0.06418475, 0.14847186, 0.27499590, 0.45493642, 0.70832630, 1.07419417, 1.64237442, 2.70554345, 2.87437340, 3.06490172, 3.28302029, 3.53738460, 3.84145882, 4.21788459, 4.70929225, 5.41189443, 6.63489660, 6.82282684, 7.03347427, 7.27296897, 7.55030254, 7.87943858, 8.28381500, 8.80746839, 9.54953571, 10.82756617, 11.02275934, 11.24123284, 11.48924675, 11.77597742, 12.11566515, 12.53219331, 13.07039414, 13.83108362, 15.13670523, 15.33569084, 15.55829202, 15.81084854, 16.10265061, 16.44811021, 16.87138911, 17.41782129, 18.18929348, 19.51142096, 19.71272662, 19.93786740, 20.19323638, 20.48820029, 20.83728702, 21.26484729, 21.81655780, 22.59504266, 23.92812698, 24.13099475, 24.35785181, 24.61512926, 24.91224854, 25.26382073, 25.69433267, 26.24970916, 27.03311129, 28.37398736, 28.57797700, 28.80606984, 29.06472539, 29.36340609, 29.71678549, 30.14945296, 30.70752084, 31.49455797, 32.84125335, 33.04608695, 33.27511121, 33.53480767, 33.83467059, 34.18942211, 34.62373144, 35.18385750, 35.97368894, 37.32489311, 37.53038421, 37.76013523, 38.02064522, 38.32143347, 38.67726154, 39.11286240, 39.67461253, 40.46665791, 41.82145620, 42.02747372, 42.25780726, 42.51897017, 42.82050279, 43.17719775, 43.61384066, 44.17690440, 44.97074640, 46.32847599, 46.53491954, 46.76572428, 47.02744599, 47.32958438, 47.68698487, 48.12447512, 48.68860480, 49.48400609, 50.84417133, 51.05105142, 51.28207860, 51.54430183, 51.84705969, 52.20477304, 52.64310208, 53.20834131, 54.00528396, 55.36641351, 55.57328236, 55.80452511, 56.06665929, 56.37286446, 56.73137415, 57.17020486, 57.73603707, 58.53368325, 59.89764960, 60.10503365, 60.33686835, 60.59971739, 60.90317599, 61.26212102, 61.70147751, 62.26797585, 63.06654055, 64.43203897, 64.66411330, 64.92723197, 65.23099918, 65.59030559, 65.59030559, 66.03009909, 66.59715246, 67.39648382, 68.76325221};
	double chisq_2DF[145] = {0.0, 0.2107210, 0.4462871, 0.7133499, 1.0216512, 1.3862944, 1.8325815, 2.4079456, 3.2188758, 4.6051702, 4.8158912, 5.0514573, 5.3185201, 5.6268214, 5.9914645, 6.4377516, 7.0131158, 7.8240460, 9.2103404, 9.4210614, 9.6566275, 9.9236903, 10.2319916, 10.5966347, 11.0429218, 11.6182860, 12.4292162, 13.8155106, 14.0262316, 14.2617977, 14.5288604, 14.8371618, 15.2018049, 15.6480920, 16.2234562, 17.0343864, 18.4206807, 18.6314018, 18.8669678, 19.1340306, 19.4423320, 19.8069751, 20.2532622, 20.8286264, 21.6395566, 23.0258509, 23.2365720, 23.4721380, 23.7392008, 24.0475022, 24.4121453, 24.8584324, 25.4337965, 26.2447268, 27.6310211, 27.8417421, 28.0773082, 28.3443710, 28.6526724, 29.0173155, 29.4636026, 30.0389667, 30.8498969, 32.2361913, 32.4469123, 32.6824784, 32.9495412, 33.2578426, 33.6224857, 34.0687728, 34.6441369, 35.4550671, 36.8413615, 37.0520825, 37.2876486, 37.5547114, 37.8630127, 38.2276559, 38.6739430, 39.2493071, 40.0602373, 41.4465317, 41.6572528, 41.8928189, 42.1598817, 42.4681828, 42.8328259, 43.2791130, 43.8544771, 44.6654073, 46.0517017, 46.2624227, 46.4979888, 46.7650516, 47.0733529, 47.4379961, 47.8842832, 48.4596473, 49.2705775, 50.6568719, 50.8675880, 51.1031479, 51.3702345, 51.6785305, 52.0431662, 52.4894422, 53.0647879, 53.8757921, 55.2620865, 55.4728815, 55.7082626, 55.9754046, 56.2838117, 56.6481588, 57.0945569, 57.6701061, 58.4814064, 59.8665906, 60.0770650, 60.3123229, 60.5789894, 60.8904629, 61.2551060, 61.7013931, 62.2767572, 63.0876874, 64.4739818, 64.6847028, 64.9202689, 65.1873317, 65.4956330, 65.8602762, 66.3065633, 66.8819274, 67.6928576, 69.0791520, 69.3147181, 69.5817808, 69.8900822, 70.2547253, 70.2547253, 70.7010124, 71.2763766, 72.0873068, 73.4736011};
	double chisqPval[181] = {1.0, 9e-01, 8e-01, 7e-01, 6e-01, 5e-01, 4e-01, 3e-01, 2e-01, 1e-01, 9e-02, 8e-02, 7e-02, 6e-02, 5e-02, 4e-02, 3e-02, 2e-02, 1e-02, 9e-03, 8e-03, 7e-03, 6e-03, 5e-03, 4e-03, 3e-03, 2e-03, 1e-03, 9e-04, 8e-04, 7e-04, 6e-04, 5e-04, 4e-04, 3e-04, 2e-04, 1e-04, 9e-05, 8e-05, 7e-05, 6e-05, 5e-05, 4e-05, 3e-05, 2e-05, 1e-05, 9e-06, 8e-06, 7e-06, 6e-06, 5e-06, 4e-06, 3e-06, 2e-06, 1e-06, 9e-07, 8e-07, 7e-07, 6e-07, 5e-07, 4e-07, 3e-07, 2e-07, 1e-07, 9e-08, 8e-08, 7e-08, 6e-08, 5e-08, 4e-08, 3e-08, 2e-08, 1e-08, 9e-09, 8e-09, 7e-09, 6e-09, 5e-09, 4e-09, 3e-09, 2e-09, 1e-09, 9e-10, 8e-10, 7e-10, 6e-10, 5e-10, 4e-10, 3e-10, 2e-10, 1e-10, 9e-11, 8e-11, 7e-11, 6e-11, 5e-11, 4e-11, 3e-11, 2e-11, 1e-11, 9e-12, 8e-12, 7e-12, 6e-12, 5e-12, 4e-12, 3e-12, 2e-12, 1e-12, 9e-13, 8e-13, 7e-13, 6e-13, 5e-13, 4e-13, 3e-13, 2e-13, 1e-13, 9e-14, 8e-14, 7e-14, 6e-14, 5e-14, 4e-14, 3e-14, 2e-14, 1e-14, 9e-15, 8e-15, 7e-15, 6e-15, 5e-15, 4e-15, 3e-15, 2e-15, 1e-15, 9e-16, 8e-16, 7e-16, 6e-16, 5e-16, 4e-16, 3e-16, 2e-16, 1e-16, 9e-17, 8e-17, 7e-17, 6e-17, 5e-17, 4e-17, 3e-17, 2e-17, 1e-17, 9e-18, 8e-18, 7e-18, 6e-18, 5e-18, 4e-18, 3e-18, 2e-18, 1e-18, 9e-19, 8e-19, 7e-19, 6e-19, 5e-19, 4e-19, 3e-19, 2e-19, 1e-19, 9e-20, 8e-20, 7e-20, 6e-20, 5e-20, 4e-20, 3e-20, 2e-20, 1e-20};

	int getSampleNumber(std::string & name);
	virtual void createStorage();
	void readPhenoFile(std::string & PhenoFileName);
	void openOutputFiles(std::string & outputName);
	virtual void writeHeader(std::ofstream & out);
	void calcMeanAndSdOfPhenotypes();
	void fillProbGenotypesHW(double & f, double* probGenotypes);
	virtual double calcProbOfPhenotype(double & phenotype, int & genotype, double* Beta, const double & Sigma2);
	virtual double calcLogLikelihood(double* thesePhenotypes, double* Beta, const double & Sigma2, double* & probGenotypes);
	virtual bool findMLENullModel(double & freqFilter);
	virtual void findMLEAlternativeModel();
	void writeLog10PValue(double & D, int DF, std::ofstream & out);
	bool performAssociationTest_core(double & freqFilter);

public:
	TPhenotypes(std::string & OutputName);
	virtual void initialize(std::string & PhenoFileName, TVcfFile_base & vcfFile); //vcf format
	virtual void initialize(std::string & PhenoFileName, TBeagleFile & beagleFile); //beagle format

	virtual ~TPhenotypes(){
		delete[] genotypeFrequencies_alt;
		delete[] genotypeFrequencies_M0;
		if(storageCreated){
			for(int p=0; p<numPhenotypes; ++p)
				delete[] phenotypes[p];
			delete[] phenotypes;
			delete[] phenotypeNames;
			for(int i=0; i<numSamples; ++i){
				delete[] genotypeLikelihoods[i];
				delete[] weights[i];
			}
			delete[] genotypeLikelihoods;
			delete[] weights;
			delete[] LL_alt;
			delete[] LL_M0;
		}

		if(sampleNamesArrayCreated) delete[] sampleNames;

		if(outputFilesOpened){
			for(int p=0; p<numPhenotypes; ++p){
				outputFiles[p].close();
			}
			delete[] outputFiles;
		}

		if(meansVariancesInitialized){
			delete[] means;
			delete[] variances;
		}
	};

	int getNumPhenotypes(){return numPhenotypes;};
	int getNumSamples(){return numSamples;};
	void setConvergenceThresholds(double EpsilonLL, double EpsilonF){epsilonLL = EpsilonLL; epsilonF = EpsilonF;};
	void updateGenotypeLikelihoods();
	bool performAssociationTest(TVcfFileSingleLine & vcfFile, double & freqFilter);
	bool performAssociationTest(TBeagleFile & beagleFile, double & freqFilter);
};

//--------------------------------------
//TPhenotypesAdditiveMLEGenotypes
//--------------------------------------
class TPhenotypesAdditiveMLEGenotypes:public TPhenotypes{
protected:
	int* MLEGenotypes;
	bool* genotypeCalled;
	int numSamplesWithGenotype;

public:
	TPhenotypesAdditiveMLEGenotypes(std::string & OutputName):TPhenotypes(OutputName){
		numSamplesWithGenotype = 0;
		genotypeCalled = NULL;
		MLEGenotypes = NULL;
	};
	~TPhenotypesAdditiveMLEGenotypes(){
		if(storageCreated){
			delete[] MLEGenotypes;
			delete[] genotypeCalled;
		}
	};
	void createStorage();
	double calcLogLikelihood(double* thesePhenotypes, double* Beta, double & Sigma2);
	double getMLEGenotypesAndCalcAlleleFrequency();
	bool findMLENullModel(double & freqFilter);
	void findMLEAlternativeModel();
};

//--------------------------------------
//TPhenotypesGeneralized
//--------------------------------------
class TPhenotypesGeneralized:public TPhenotypes{
protected:

public:
	TPhenotypesGeneralized(std::string & OutputName):TPhenotypes(OutputName){};
	void writeHeader(std::ofstream & out);
	double calcProbOfPhenotype(double & phenotype, int & genotype, double* Beta, double & Sigma2);
	void findMLEAlternativeModel();
};


/*
//--------------------------------------
//TPhenotypesLogit
//--------------------------------------
class TPhenotypesLogit:public TPhenotypes{
protected:
	int numParams;
	double** x_g;
	double** J;
	double** J_inv;
	double* F;
	double* beta_null;
	double* beta;
	double* beta_old;

	bool testedForPhenotypesBeingBinary;

	//convergence param
	double epsilonNR;


	void checkForBinaryPhenotypes();
	void createParamStorage();
	void createStorage();
	void writeHeader(std::ofstream & out);
	double logit(double z);
	double calcProbOfPhenotypeLogit(int genotype, double* Beta);
	double calcLogLikelihoodLogit(double* thesePhenotypes, double* Beta, double* & probGenotypes);
	void findMLENullModel(double & initialF);
	void inverseJ();
	double calculateWeightsAndEstimateFAlternativeModel(int & phenotype);
	void runNewtonRaphson(int & phenotype);
	void findMLEAlternativeModel();

public:
	TPhenotypesLogit(std::string & OutputName);
	~TPhenotypesLogit(){
		for(int g=0; g<3; ++g)
			delete[] x_g[g];
		delete[] x_g;
		for(int j=0; j<numParams; ++j){
			delete[] J[j];
			delete[] J_inv[j];
		}
		delete[] J;
		delete[] J_inv;
		delete[] F;
		delete[] beta;
		delete[] beta_old;
		delete[] beta_null;
	};

	void initialize(std::string & PhenoFileName, TVcfFile_base & vcfFile); //vcf format
	void initialize(std::string & PhenoFileName, TBeagleFile & beagleFile); //beagle format

};
*/

#endif /* TPHENOTYPES_H_ */

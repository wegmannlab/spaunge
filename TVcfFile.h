/*
 * TVcfFile.h
 *
 *  Created on: Aug 8, 2011
 *      Author: wegmannd
 */

#ifndef TVCFFILE_H_
#define TVCFFILE_H_

#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include "TVcfParser.h"
#include <algorithm>
#include "TLog.h"
#include "gzstream.h"

typedef void (TVcfParser::*pt2Function)(TVcfLine &);
//---------------------------------------------------------------------------------------------------------
class TVcfFile_base{
public:
	std::istream* myStream;
	bool inputStreamOpend;
	std::ostream* myOutStream;
	bool outputStreamOpend;
	std::string fileFormat;
	TVcfColumnNumbers cols;
	TVcfParser parser;
	unsigned int numCols;
	long currentLine;
	std::vector< pt2Function > usedParsers;
	bool automaticallyWriteVcf;
	TVcfLine* tempLine;
	bool eof;
	double totalFileSize;

	//vector<TVcfFilter> filters;
	//bool applyFilters;

	std::vector<std::string> unknownHeader;

	TVcfFile_base(){currentLine=0; automaticallyWriteVcf=false;eof=false;numCols=-1;totalFileSize=-1;myOutStream=NULL; myStream=NULL; inputStreamOpend=false; outputStreamOpend=false; tempLine=NULL;};
	TVcfFile_base(std::string & filename, bool zipped);
	virtual ~TVcfFile_base(){
		if(inputStreamOpend) delete myStream;
		if(outputStreamOpend) delete myOutStream;
	};
	void enableAutomaticWriting(){
		if(!outputStreamOpend)
			throw "Can not automatically write VCF: no output stream has been opened!";
		automaticallyWriteVcf = true;
	};
	void openStream(std::string & filename, bool zipped);
	void openOutputStream(std::string & filename, bool zipped);
	void setOutStream(std::ostream & is);

	//which parsers to use?
	void enablePositionParsing(){usedParsers.push_back(&TVcfParser::parsePosition);};
	void enableVariantParsing(){usedParsers.push_back(&TVcfParser::parseVariant);};
	void enableInfoParsing(){usedParsers.push_back(&TVcfParser::parseInfo);};
	void enableFormatParsing(){usedParsers.push_back(&TVcfParser::parseFormat);};
	void enableSampleParsing(){usedParsers.push_back(&TVcfParser::parseSamples);};

	//retrieve info
	GTLikelihoods genotypeLiklihoods(TVcfLine* line, unsigned int sample);
	void fillGenotypeLiklihoods(TVcfLine* line, unsigned int sample, double* gtl);
	int sampleNumber(std::string & Name);
	int numSamples();
	std::string sampleName(unsigned int num);
	bool sampleIsMissing(TVcfLine* line, unsigned int sample);
	virtual std::string fieldContentAsString(std::string tag, TVcfLine* line, unsigned int sample);
	virtual int fieldContentAsInt(std::string tag, TVcfLine* line, unsigned int sample);

	//modify
	void setSampleMissing(TVcfLine* line, unsigned int sample);

	//void addFilter(my_string filter);
	//void filterSamples();
	//void printFilters();

	void parseHeaderVCF_4_0();
	void writeHeaderVCF_4_0();
	void addNewHeaderLine(std::string headerLine);
	bool readLine();
	void addFormat(std::string Line){parser.addFormat(Line);};

	//modify header and columns
	void updateInfo(TVcfLine* line, std::string Id, std::string Data);
	void addToInfo(TVcfLine* line, std::string Id, std::string Data);
};

class TVcfFileSingleLine:public TVcfFile_base{
public:
	bool written;

	TVcfFileSingleLine(){written=true;};
	TVcfFileSingleLine(std::string & filename, bool zipped);
	TVcfFileSingleLine(TLog* Logfile);
	virtual ~TVcfFileSingleLine();
	void writeLine();
	bool next();
	//call specific parsers
	void parseInfo(){ parser.parseSamples(*tempLine); };
	void parseFormat(){ parser.parseFormat(*tempLine); };
	void parseSamples(){ parser.parseSamples(*tempLine); };
	//other stuff
	TVcfLine* pointerToVcfLine(){return tempLine;};
	void updateInfo(std::string Id, std::string Data);
	void addToInfo(std::string Id, std::string Data);
	virtual std::string fieldContentAsString(std::string tag, unsigned int sample);
	virtual int fieldContentAsInt(std::string tag, unsigned int sample);
	GTLikelihoods genotypeLikelihoods(unsigned int sample);
	void fillGenotypeLikelihoods(unsigned int sample, double* gtl);
	//variant info
	long position();
	std::string chr();
	int getNumAlleles();
	std::string getRefAllele();
	std::string getFirstAltAllele();
	std::string getAllele(int num);
	//sampel info
	void setSampleMissing(unsigned int sample);
	bool sampleIsMissing(unsigned int sample);
	bool sampleIsHomoRef(unsigned int sample);
	bool sampleIsHeteroRefNonref(unsigned int sample);
	std::string getFirstAlleleOfSample(unsigned int num);
	std::string getSecondAlleleOfSample(unsigned int num);
	float sampleGenotypeQuality(unsigned int sample);
	int sampleDepth(unsigned int sample);
};


#endif /* TVCFFILE_H_ */

/*
 * TBeagleFile.h
 *
 *  Created on: Feb 23, 2016
 *      Author: phaentu
 */

#ifndef TBEAGLEFILE_H_
#define TBEAGLEFILE_H_

#include "stringFunctions.h"
#include <iostream>
#include <fstream>
#include "gzstream.h"

class TBeagleFile{
private:
	bool isZipped, streamOpend;
	std::istream* myStream;
	std::ostream* myOutStream;
	bool outputStreamOpend;
	std::vector<std::string> vec;
	std::vector<std::string> header;
	std::vector<std::string>::iterator it;
	std::string tmp, tmp2;
	int numSamplesInFile;
	std::string* sampleNames;
	bool eof;
	long currentLine;

	bool readLine(std::vector<std::string> & thisVec);

public:
	TBeagleFile();
	TBeagleFile(std::string & filename, bool zipped);
	void initialize(std::string & filename, bool zipped);
	~TBeagleFile(){
		delete[] sampleNames;
		if(streamOpend) delete myStream;
		if(outputStreamOpend) delete myOutStream;
	};
	void openOutputStream(std::string & filename, bool zipped);
	bool next();
	void writeHeader();
	void writeLine();
	void fillGenotypeLikelihoods(double** genotypeLikelihoods);
	std::string getChrPosString();
	int numSamples(){return numSamplesInFile;};
	std::string* getPointerToSampleNames(){return sampleNames;};
	long getCurrentLine(){return currentLine;};
};



#endif /* TBEAGLEFILE_H_ */

/*
 * TSpaunge_core.h
 *
 *  Created on: Feb 19, 2016
 *      Author: wegmannd
 */

#ifndef TSPAUNGE_CORE_H_
#define TSPAUNGE_CORE_H_

#include "TLog.h"
#include "TParameters.h"
#include "TRandomGenerator.h"
#include "TVcfFile.h"
#include <algorithm>
#include "TPhenotypes.h"
#include "TLinkageModel.h"
#include "gzstream.h"


//--------------------------------------
//TSpaunge_core
//--------------------------------------
class TSpaunge_core{
private:
	TLog* logfile;
	TRandomGenerator* randomGenerator;
	bool randomGeneratorInitialized;
	std::string outputName;
	TVcfFileSingleLine vcfFile;
	TBeagleFile beagleFile;
	bool isVCF, isBeagle, isZipped;
	char bases[4] = {'A', 'C', 'G', 'T'};


	void initializeRandomGenerator(TParameters & params);
	void prepareInput(TParameters & params);

	//inference
	void printProgress(long lines, long & tests, struct timeval & start);
	void spotAssociationsVCF(TPhenotypes* phenotypes, double & freqFilter, int & progressFrequency);
	void spotAssociationsBeagle(TPhenotypes* phenotypes, double & freqFilter, int & progressFrequency);
	void printProgressFrequencyFiltering(long lines, long & numRetainedLoci, struct timeval & start);
	void filterOnFrequencyVCF(double & epsilonF, double & freqFilter, int & progressFrequency, gz::ogzstream & mafFile);
	void filterOnFrequencyBeagle(double & epsilonF, double & freqFilter, int & progressFrequency, gz::ogzstream & mafFile);

public:


	TSpaunge_core(TLog* Logfile, TParameters & params);
	~TSpaunge_core(){
		if(randomGeneratorInitialized) delete randomGenerator;
	};

	void simulateGWASData(TParameters & params);
	void spotAssociations(TParameters & params);
	void filterOnFrequency(TParameters & params);
	void inferLinkageModel(TParameters & params);

};



#endif /* TSPAUNGE_CORE_H_ */

/*
 * TLinkageModel.cpp
 *
 *  Created on: Nov 1, 2016
 *      Author: wegmannd
 */

#include "TLinkageModel.h"

TLinkageModelEstimator::TLinkageModelEstimator(TParameters & params, TLog* Logfile, TBeagleFile* BeagleFile){
	beagleFile = BeagleFile;
	isBeagle = true; isVCF = false;
	numInd = beagleFile->numSamples();
	initialize(params, Logfile);
}

TLinkageModelEstimator::TLinkageModelEstimator(TParameters & params, TLog* Logfile, TVcfFile_base* VcfFile){
	vcfFile = VcfFile;
	isBeagle = false; isVCF = true;
	numInd = vcfFile->numSamples();
	initialize(params, Logfile);
}

void TLinkageModelEstimator::initialize(TParameters & params, TLog* Logfile){
	logfile = Logfile;

	//read variables
	logfile->startIndent("Reading parameters:");
	windowSize = params.getParameterIntWithDefault("block", 1000);
	logfile->list("Estimating linkage model in blocks of " + toString(windowSize) + " variants");
	overlap = params.getParameterIntWithDefault("overlap", 100);
	logfile->list("Blocks will overlap by " + toString(overlap) + " variants");
	halfOverlap = overlap / 2;
	if(overlap > windowSize / 3)
		throw "Overlap can not be larger than one third of window size! Increase block size.";

	blockSize_K = params.getParameterInt("K");

	freqFilter = -1.0;
	if(params.parameterExists("freqFilter")){
		freqFilter = params.getParameterDoubleWithDefault("freqFilter", 0.01);
		logfile->list("Will filter out variants with frequencies < " + toString(freqFilter));
	}

	maxNumBWIterations = params.getParameterDoubleWithDefault("maxBWIterations", 100);
	maxDeltaLL = params.getParameterDoubleWithDefault("deltaLL", 0.0001);
	logfile->startIndent("Will terminate Baum-Welch algorithm:");
	logfile->list("If delta LL < " + toString(maxDeltaLL));
	logfile->list("After at max " + toString(maxNumBWIterations) + " iterations");
	logfile->endIndent();

	deltaF = params.getParameterDoubleWithDefault("epsF", 0.0001);
	logfile->list("Will run Newton-Raphson to estimate allele frequencies until delta F < " +toString(deltaF));
	logfile->endIndent();

	//initialize storage (individual, loci, genotype)
	locusName = new std::string[windowSize];
	genotypeLikelihoods = new double**[numInd];
	for(int i=0; i<numInd; ++i){
		genotypeLikelihoods[i] = new double*[windowSize];
		for(int t=0; t<windowSize; ++t)
			genotypeLikelihoods[i][t] = new double[3];
	}
	lastSiteWithDataPlusOne = 0;

	//set num states
	numStates_B = pow(2, blockSize_K);
	twoPowKMinusOne = numStates_B / 2;
	numStates_BB = numStates_B * numStates_B;

	//initialize HMM variables
	alpha = new double**[windowSize];
	r = new double**[windowSize];
	r_new = new double*[windowSize];
	r_new_denominator = new double*[windowSize];
	for(int l=0; l<windowSize; ++l){
		alpha[l] = new double*[numStates_B];
		r[l] = new double*[numStates_B];
		r_new[l] = new double[numStates_B];
		r_new_denominator[l] = new double[numStates_B];
		for(int b1=0; b1<numStates_B; ++b1){
			alpha[l][b1] = new double[numStates_B];
			r[l][b1] = new double[2];
		}
	}

	//beta, gamma and xi
	beta = new double**[2];
	beta[0] = new double*[numStates_B];
	beta[1] = new double*[numStates_B];
	xi = new double**[numStates_B];
	for(int b1=0; b1<numStates_B; ++b1){
		beta[0][b1] = new double[numStates_B];
		beta[1][b1] = new double[numStates_B];
		xi[b1] = new double*[numStates_B];
		for(int b2=0; b2<numStates_B; ++b2){
			xi[b1][b2] = new double[4];
		}
	}

	//gamma
	gamma = new double**[windowSize];
	for(int l=0; l<windowSize; ++l){
		gamma[l] = new double*[numStates_B];
		for(int b1=0; b1<numStates_B; ++b1){
			gamma[l][b1] = new double[numStates_B];
		}
	}

	//zeta
	zeta = new double**[numStates_B];
	zetaProd = new double****[2];
	zetaProd[0] = new double***[numStates_B];
	zetaProd[1] = new double***[numStates_B];
	for(int b1=0; b1<numStates_B; ++b1){
		zeta[b1] = new double*[numStates_B];
		zetaProd[0][b1] = new double**[numStates_B];
		zetaProd[1][b1] = new double**[numStates_B];
		for(int b2=0; b2<numStates_B; ++b2){
			zeta[b1][b2] = new double[4];
			zetaProd[0][b1][b2] = new double*[numStates_B];
			zetaProd[1][b1][b2] = new double*[numStates_B];
			for(int b3=0; b3<numStates_B; ++b3){
				zetaProd[0][b1][b2][b3] = new double[numStates_B];
				zetaProd[1][b1][b2][b3] = new double[numStates_B];
			}
		}
	}


	//table to lookup next and previous states
	nextState = new int*[numStates_B];
	previousState = new int*[numStates_B];
	for(int b=0; b<numStates_B; ++b){
		//next
		nextState[b] = new int[2];
		nextState[b][0] = (2*b) % numStates_B;
		nextState[b][1] = nextState[b][0] + 1;

		//previous
		previousState[b] = new int[2];
		previousState[b][0] = b / 2;
		previousState[b][1] = previousState[b][0] + twoPowKMinusOne;
	}
}

TLinkageModelEstimator::~TLinkageModelEstimator(){
	for(int i=0; i<numInd; ++i){
		for(int t=0; t<windowSize; ++t)
			delete[] genotypeLikelihoods[i][t];
		delete[] genotypeLikelihoods[i];
	}
	delete[] genotypeLikelihoods;
	delete[] locusName;

	for(int l=0; l<windowSize; ++l){
		for(int b1=0; b1<numStates_B; ++b1){
			delete[] alpha[l][b1];
			delete[] r[l][b1];
		}
		delete[] alpha[l];
		delete[] r[l];
		delete[] r_new[l];
		delete[] r_new_denominator[l];
	}
	delete[] alpha;
	delete[] r;
	delete[] r_new;
	delete[] r_new_denominator;

	for(int b1=0; b1<numStates_B; ++b1){
		delete[] beta[0][b1];
		delete[] beta[1][b1];
		for(int b2=0; b2<numStates_B; ++b2){
			delete[] xi[b1][b2];
		}
		delete[] xi[b1];
	}
	delete[] beta[0];
	delete[] beta[1];
	delete[] xi;

	//gamma
	for(int l=0; l<windowSize; ++l){
		for(int b1=0; b1<numStates_B; ++b1){
			delete[] gamma[l][b1];
		}
		delete[] gamma[l];
	}

	//zeta
	for(int b1=0; b1<numStates_B; ++b1){
		for(int b2=0; b2<numStates_B; ++b2){
			for(int b3=0; b3<numStates_B; ++b3){
				delete[] zetaProd[b1][b2][b3];
			}
			delete[] zeta[b1][b2];
			delete[] zetaProd[b1][b2];
		}
		delete[] zeta[b1];
		delete[] zetaProd[b1];
	}
	delete[] zeta;
	delete[] zetaProd;

}


void TLinkageModelEstimator::readData(){
	double** tmp = new double*[numInd];
	for(int i=0; i<numInd; ++i)
		tmp[i] = new double[3];

	//read data of a full window into genotypeLikelihoods
	if(isVCF){

	} else if(isBeagle){
		//read beagle file until storage is full
		int l = 0;
		while(l < windowSize && beagleFile->next()){
			beagleFile->fillGenotypeLikelihoods(tmp);
			//transpose
			for(int i=0; i<numInd; ++i){
				genotypeLikelihoods[i][l][0] = tmp[i][0];
				genotypeLikelihoods[i][l][1] = tmp[i][1];
				genotypeLikelihoods[i][l][2] = tmp[i][2];
			}
			locusName[l] = beagleFile->getChrPosString();
			++l;
		}
		lastSiteWithDataPlusOne = l;
	} else
		throw "Unknown format!";

	//clean up
	for(int i=0; i<numInd; ++i)
		delete[] tmp[i];
	delete[] tmp;

}

double TLinkageModelEstimator::runForwardBackward(){
	//some variables
	int b1_is_one, b2_is_one;
	int lMinusOne, lPlusOne;
	double tmp;
	double normSum;
	int l, b1, b2;
	double LL = 0.0;

	//set new r estimates = 0 and initializ
	for(l=0; l<lastSiteWithDataPlusOne; ++l){
		for(b1=0; b1<numStates_B; ++b1){
			r_new[l][b1] = 0.0;
			r_new_denominator[l][b1] = 0.0;
		}
	}

	logfile->listFlush("Looping over individuals ...");
	int prog = 0;
	int oldProg = 0;

	//-----------------------------
	//now loop over all individuals
	//-----------------------------
	for(int i=0; i<numInd; ++i){
		//1) run forward to get alphas
		//----------------------------
		//first for locus l=0
		normSum = 1.0 / numStates_BB;
		for(int b1=0; b1<numStates_B; ++b1){
			for(int b2=0; b2<numStates_B; ++b2){
				alpha[0][b1][b2] = 0.0;
			}
		}

		alpha[0][0][0] = r[0][0][0] * r[0][0][0] * genotypeLikelihoods[i][0][0];
		alpha[0][0][1] = r[0][0][0] * r[0][0][1] * genotypeLikelihoods[i][0][1];
		alpha[0][1][0] = r[0][0][1] * r[0][0][0] * genotypeLikelihoods[i][0][1];
		alpha[0][1][1] = r[0][0][1] * r[0][0][1] * genotypeLikelihoods[i][0][2];
		normSum = alpha[0][0][0] + alpha[0][0][1] + alpha[0][1][0] + alpha[0][1][1];

		alpha[0][0][0] /= normSum;
		alpha[0][0][1] /= normSum;
		alpha[0][1][0] /= normSum;
		alpha[0][1][1] /= normSum;

		LL += log(normSum);

		//now for all other loci
		for(l=1; l<lastSiteWithDataPlusOne; ++l){
			lMinusOne = l - 1;
			normSum = 0.0;

			for(b1=0; b1<numStates_B; ++b1){
				b1_is_one = b1 % 2;
				for(b2=0; b2<numStates_B; ++b2){
					b2_is_one = b2 % 2;
					tmp = 0.0;

					//only some over the two possible previous states per chain
					tmp += alpha[lMinusOne][previousState[b1][0]][previousState[b2][0]] * r[l][previousState[b1][0]][b1_is_one] * r[l][previousState[b2][0]][b2_is_one];
					tmp += alpha[lMinusOne][previousState[b1][0]][previousState[b2][1]] * r[l][previousState[b1][0]][b1_is_one] * r[l][previousState[b2][1]][b2_is_one];
					tmp += alpha[lMinusOne][previousState[b1][1]][previousState[b2][0]] * r[l][previousState[b1][1]][b1_is_one] * r[l][previousState[b2][0]][b2_is_one];
					tmp += alpha[lMinusOne][previousState[b1][1]][previousState[b2][1]] * r[l][previousState[b1][1]][b1_is_one] * r[l][previousState[b2][1]][b2_is_one];
					alpha[l][b1][b2] = tmp * genotypeLikelihoods[i][l][b1_is_one+b2_is_one];
					normSum += alpha[l][b1][b2];
				}
			}


			for(int b1=0; b1<numStates_B; ++b1){
				for(int b2=0; b2<numStates_B; ++b2){
					alpha[l][b1][b2] /= normSum;
				}
			}

			LL += log(normSum);
		}

		//2) run backward to get betas
		//----------------------------
		//fill for last locus
		int t = 0;
		int tPlusOne = 1;
		lMinusOne = lastSiteWithDataPlusOne - 2;
		l = lastSiteWithDataPlusOne - 1;
		tmp = 1.0 / numStates_BB;
		normSum = 0.0;

		/*
		//calc gammas
		normSum = 0.0;
		for(int b1=0; b1<numStates_B; ++b1){
			for(int b2=0; b2<numStates_B; ++b2){
				gamma[l][b1][b2] = alpha[l][b1][b2];
			}
		}
		*/

		for(b1=0; b1<numStates_B; ++b1){
			for(b2=0; b2<numStates_B; ++b2){
				//set beta = 1
				beta[tPlusOne][b1][b2] = tmp;

				//calculate xi
				xi[b1][b2][0] = alpha[lMinusOne][b1][b2] * r[l][b1][0] * r[l][b2][0] * genotypeLikelihoods[i][l][0];
				xi[b1][b2][1] = alpha[lMinusOne][b1][b2] * r[l][b1][0] * r[l][b2][1] * genotypeLikelihoods[i][l][1];
				xi[b1][b2][2] = alpha[lMinusOne][b1][b2] * r[l][b1][1] * r[l][b2][0] * genotypeLikelihoods[i][l][1];
				xi[b1][b2][3] = alpha[lMinusOne][b1][b2] * r[l][b1][1] * r[l][b2][1] * genotypeLikelihoods[i][l][2];
				normSum += xi[b1][b2][0] + xi[b1][b2][1] + xi[b1][b2][2] + xi[b1][b2][3];
			}
		}

		//add to sums
		for(b1=0; b1<numStates_B; ++b1){
			for(b2=0; b2<numStates_B; ++b2){
				tmp = (xi[b1][b2][0] + xi[b1][b2][1] + xi[b2][b1][0] + xi[b2][b1][2]) / normSum;
				r_new[l][b1] += tmp;
				r_new_denominator[l][b1] += tmp + (xi[b1][b2][3] + xi[b1][b2][2] + xi[b2][b1][3] + xi[b2][b1][1]) / normSum;
			}
		}

		/*
		//calc last zeta
		for(b1=0; b1<numStates_B; ++b1){
			for(b2=0; b2<numStates_B; ++b2){
				for(int b3=0; b3<numStates_B; ++b3){
					for(int b4=0; b4<numStates_B; ++b4){
						zetaProd[tPlusOne][b1][b2][b3][b4] = 0.0;
					}
				}

				tmp = xi[b1][b2][0] + xi[b1][b2][1] + xi[b1][b2][2] + xi[b1][b2][3];
				zetaProd[tPlusOne][b1][b2][nextState[b1][0]][nextState[b2][0]] = xi[b1][b2][0] / tmp;
				zetaProd[tPlusOne][b1][b2][nextState[b1][0]][nextState[b2][1]] = xi[b1][b2][1] / tmp;
				zetaProd[tPlusOne][b1][b2][nextState[b1][1]][nextState[b2][0]] = xi[b1][b2][2] / tmp;
				zetaProd[tPlusOne][b1][b2][nextState[b1][1]][nextState[b2][1]] = xi[b1][b2][3] / tmp;
			}
		}
		 */

		//all other locus (except first)
		for(l=lastSiteWithDataPlusOne-2; l>0; --l){
			lPlusOne = l+1;
			lMinusOne = l-1;
			normSum = 0.0;
			for(int b1=0; b1<numStates_B; ++b1){
				for(int b2=0; b2<numStates_B; ++b2){
					beta[t][b1][b2] = 0.0;

					//only some over the two possible previous states per chain
					beta[t][b1][b2] += beta[tPlusOne][nextState[b1][0]][nextState[b2][0]] * r[lPlusOne][b1][0] * r[lPlusOne][b2][0] * genotypeLikelihoods[i][lPlusOne][0];
					beta[t][b1][b2] += beta[tPlusOne][nextState[b1][0]][nextState[b2][1]] * r[lPlusOne][b1][0] * r[lPlusOne][b2][1] * genotypeLikelihoods[i][lPlusOne][1];
					beta[t][b1][b2] += beta[tPlusOne][nextState[b1][1]][nextState[b2][0]] * r[lPlusOne][b1][1] * r[lPlusOne][b2][0] * genotypeLikelihoods[i][lPlusOne][1];
					beta[t][b1][b2] += beta[tPlusOne][nextState[b1][1]][nextState[b2][1]] * r[lPlusOne][b1][1] * r[lPlusOne][b2][1] * genotypeLikelihoods[i][lPlusOne][2];

					normSum += beta[t][b1][b2];
				}
			}

			//normalize beta
			for(int b1=0; b1<numStates_B; ++b1){
				for(int b2=0; b2<numStates_B; ++b2){
					beta[t][b1][b2] = beta[t][b1][b2] / normSum;
				}
			}

			//calc gammas
			/*
			normSum = 0.0;
			for(int b1=0; b1<numStates_B; ++b1){
				for(int b2=0; b2<numStates_B; ++b2){
					gamma[l][b1][b2] = alpha[l][b1][b2] * beta[t][b1][b2];
					normSum += gamma[l][b1][b2];
				}
			}
			for(int b1=0; b1<numStates_B; ++b1){
				for(int b2=0; b2<numStates_B; ++b2){
					gamma[l][b1][b2] /= normSum;
				}
			}
			*/

			//calc xi
			normSum = 0.0;
			for(int b1=0; b1<numStates_B; ++b1){
				for(int b2=0; b2<numStates_B; ++b2){
					xi[b1][b2][0] = alpha[lMinusOne][b1][b2] * r[l][b1][0] * r[l][b2][0] * beta[t][nextState[b1][0]][nextState[b2][0]] * genotypeLikelihoods[i][l][0];
					xi[b1][b2][1] = alpha[lMinusOne][b1][b2] * r[l][b1][0] * r[l][b2][1] * beta[t][nextState[b1][0]][nextState[b2][1]] * genotypeLikelihoods[i][l][1];
					xi[b1][b2][2] = alpha[lMinusOne][b1][b2] * r[l][b1][1] * r[l][b2][0] * beta[t][nextState[b1][1]][nextState[b2][0]] * genotypeLikelihoods[i][l][1];
					xi[b1][b2][3] = alpha[lMinusOne][b1][b2] * r[l][b1][1] * r[l][b2][1] * beta[t][nextState[b1][1]][nextState[b2][1]] * genotypeLikelihoods[i][l][2];
					normSum += xi[b1][b2][0] + xi[b1][b2][1] + xi[b1][b2][2] + xi[b1][b2][3];
				}

			}

			//add to sums
			for(int b1=0; b1<numStates_B; ++b1){
				for(int b2=0; b2<numStates_B; ++b2){
					tmp = (xi[b1][b2][0] + xi[b1][b2][1] + xi[b2][b1][0] + xi[b2][b1][2]) / normSum;
					r_new[l][b1] += tmp;
					r_new_denominator[l][b1] += tmp + (xi[b1][b2][3] + xi[b1][b2][2] + xi[b2][b1][3] + xi[b2][b1][1]) / normSum;
				}
			}

			/*
			//calc last zeta
			for(b1=0; b1<numStates_B; ++b1){
				for(b2=0; b2<numStates_B; ++b2){
					tmp = xi[b1][b2][0] + xi[b1][b2][1] + xi[b1][b2][2] + xi[b1][b2][3];
					zeta[b1][b2][0] = xi[b1][b2][0] / tmp;
					zeta[b1][b2][1] = xi[b1][b2][1] / tmp;
					zeta[b1][b2][2] = xi[b1][b2][2] / tmp;
					zeta[b1][b2][3] = xi[b1][b2][3] / tmp;

					for(int b3=0; b3<numStates_B; ++b3){
						for(int b4=0; b4<numStates_B; ++b4){
							zetaProd[t][b1][b2][b3][b4] = zeta[b1][b2][0] * zetaProd[tPlusOne][nextState[b1][0]][nextState[b2][0]][b3][b4]
														+ zeta[b1][b2][1] * zetaProd[tPlusOne][nextState[b1][0]][nextState[b2][1]][b3][b4]
														+ zeta[b1][b2][2] * zetaProd[tPlusOne][nextState[b1][1]][nextState[b2][0]][b3][b4]
														+ zeta[b1][b2][3] * zetaProd[tPlusOne][nextState[b1][1]][nextState[b2][1]][b3][b4];
						}
					}
				}
			}
			*/

			//swap betas
			tPlusOne = t;
			t = (t + 1) % 2;
		}


		//first state
		lPlusOne = 1;
		normSum = 0.0;
		for(int b1=0; b1<numStates_B; ++b1){
			for(int b2=0; b2<numStates_B; ++b2){
				beta[t][b1][b2] = 0.0;

				//only some over the two possible previous states per chain
				beta[t][b1][b2] += beta[tPlusOne][nextState[b1][0]][nextState[b2][0]] * r[lPlusOne][b1][0] * r[lPlusOne][b2][0] * genotypeLikelihoods[i][lPlusOne][0];
				beta[t][b1][b2] += beta[tPlusOne][nextState[b1][0]][nextState[b2][1]] * r[lPlusOne][b1][0] * r[lPlusOne][b2][1] * genotypeLikelihoods[i][lPlusOne][1];
				beta[t][b1][b2] += beta[tPlusOne][nextState[b1][1]][nextState[b2][0]] * r[lPlusOne][b1][1] * r[lPlusOne][b2][0] * genotypeLikelihoods[i][lPlusOne][1];
				beta[t][b1][b2] += beta[tPlusOne][nextState[b1][1]][nextState[b2][1]] * r[lPlusOne][b1][1] * r[lPlusOne][b2][1] * genotypeLikelihoods[i][lPlusOne][2];

				normSum += beta[t][b1][b2];
			}
		}

		//normalize beta
		for(int b1=0; b1<numStates_B; ++b1){
			for(int b2=0; b2<numStates_B; ++b2){
				beta[t][b1][b2] = beta[t][b1][b2] / normSum;
			}
		}

		//calc xi
		normSum = 0.0;
		xi[0][0][0] = r[l][0][0] * r[l][0][0] * beta[t][nextState[0][0]][nextState[0][0]] * genotypeLikelihoods[i][l][0];
		xi[0][0][1] = r[l][0][0] * r[l][0][1] * beta[t][nextState[0][0]][nextState[0][1]] * genotypeLikelihoods[i][l][1];
		xi[0][0][2] = r[l][0][1] * r[l][0][0] * beta[t][nextState[0][1]][nextState[0][0]] * genotypeLikelihoods[i][l][1];
		xi[0][0][3] = r[l][0][1] * r[l][0][1] * beta[t][nextState[0][1]][nextState[0][1]] * genotypeLikelihoods[i][l][2];
		normSum += xi[0][0][0] + xi[0][0][1] + xi[0][0][2] + xi[0][0][3];

		tmp = (xi[0][0][0] + xi[0][0][1] + xi[0][0][0] + xi[0][0][2]) / normSum;
		r_new[0][0] += tmp;
		r_new_denominator[0][0] += tmp + (xi[0][0][3] + xi[0][0][2] + xi[0][0][3] + xi[0][0][1]) / normSum;

		//report progress
		prog = 100 * i / (double) numInd;
		if(prog > oldProg){
			oldProg = prog;
			logfile->listOverFlush("Looping over individuals ... (" + toString(prog) + "%)");
		}
	}
	//--------------------------
	// end loop over individuals
	//--------------------------
	logfile->overList("Looping over individuals ... done!");

	//estimate new r
	logfile->listFlush("Estimating new r ...");
	for(l=0; l<lastSiteWithDataPlusOne; ++l){
		for(b1=0; b1<numStates_B; ++b1){
			if(r_new_denominator[l][b1] < 1.0E-20) r[l][b1][0] = 0.01;
			else r[l][b1][0] = r_new[l][b1] / r_new_denominator[l][b1];
			r[l][b1][1] = 1.0 - r[l][b1][0];
		}
	}
	logfile->write(" done!");

	//return LL
	return LL;
}

void TLinkageModelEstimator::estimate(std::string & outputName){
	//open output file
	std::string filename = outputName + "_rEstimates.txt.gz";
	gz::ogzstream outfile(filename.c_str());

	//write header
	if(isBeagle) outfile << "chr\tlocus";
	else if(isVCF) outfile << "chr\tpos";
	else throw "Unknown file type!";
	for(int b1=0; b1<numStates_B; ++b1)
		outfile << "\tr_" << b1;
	outfile << std::endl;

	//some variables
	double curDeltaLL;

	//find a way to parse over chromosomes window by window
	readData();

	//set all r = 0.5
	for(int l=0; l<lastSiteWithDataPlusOne; ++l){
		for(int b1=0; b1<numStates_B; ++b1){
			r[l][b1][0] = 0.5;
			r[l][b1][1] = 0.5;
		}
	}

	//run Baum Welch
	double LL, oldLL;
	for(int x = 0; x < maxNumBWIterations; ++x){
		logfile->startIndent("Running Baum-Welch iteration " + toString(x+1));
		LL = runForwardBackward();

		//decide if we stop
		logfile->list("Current LL = " + toString(LL));
		curDeltaLL = LL - oldLL;
		if(x > 1){
			if(LL - oldLL < maxDeltaLL){
				logfile->conclude("Baum-Welch has converged: deltaLL  = " + toString(curDeltaLL) + " < " + toString(maxDeltaLL));
				break;
			} else {
				logfile->conclude("Continuing: deltaLL = " + toString(curDeltaLL) + " > " + toString(maxDeltaLL));
			}
		}
		oldLL = LL;
		logfile->endIndent();
	}

	//writing estimates to file
	writeCurrentEstimatesToFile(outfile);

	//calculate pairwise correlations
	calcPairwiseCorrelations(outputName);

	//close output file
	outfile.close();
}

void TLinkageModelEstimator::writeCurrentEstimatesToFile(gz::ogzstream & outfile){
	for(int l=0; l<lastSiteWithDataPlusOne; ++l){
		outfile << locusName[l];
		for(int b1=0; b1<numStates_B; ++b1)
			outfile << "\t" << r[l][b1][0];
		outfile << "\n";
	}
}


void TLinkageModelEstimator::calcPairwiseCorrelations(std::string & outputName){
	//calculate stationary probabilities
	double** pi = new double*[lastSiteWithDataPlusOne];
	for(int l=0; l<lastSiteWithDataPlusOne; ++l)
		pi[l] = new double[numStates_B];
	double* variance = new double[lastSiteWithDataPlusOne];

	//first
	for(int b=0; b<numStates_B; ++b)
		pi[0][b] = 0.0;
	pi[0][0] = r[0][0][0];
	pi[0][1] = r[0][0][1];

	//calculate frequencies
	variance[0] = pi[0][1] * (1.0 - pi[0][1]);

	std::cout << 0 << " -> " << pi[0][0] << " / " << pi[0][1] << std::endl;

	//all other
	int lMinusOne;
	for(int l=1; l<lastSiteWithDataPlusOne; ++l){
		lMinusOne = l - 1;
		for(int b=0; b<numStates_B; ++b){
			pi[l][b] = pi[lMinusOne][previousState[b][0]] * r[l][previousState[b][0]][b % 2] + pi[lMinusOne][previousState[b][1]] * r[l][previousState[b][1]][b % 2];
		}

		std::cout << l << " -> " << pi[l][0] << " / " << pi[l][1] << std::endl;

		//calculate frequencies
		variance[l] = 0.0;
		for(int b1=1; b1<numStates_B; b1 = b1 + 2)
			variance[l] += pi[l][b1];
		variance[l] *= (1.0 - variance[l]);
	}



	//calculate pairwise correlations
	double** rho = new double*[lastSiteWithDataPlusOne];
	for(int l=0; l<lastSiteWithDataPlusOne; ++l)
		rho[l] = new double[lastSiteWithDataPlusOne];

	double** Q = new double*[numStates_B];
	double** Q_old = new double*[numStates_B];
	double** tmpQ;
	for(int b=0; b<numStates_B; ++b){
		Q[b] = new double[numStates_B];
		Q_old[b] = new double[numStates_B];
	}

	//start locus
	double D;
	double freqAtL;
	double freqAtL2;
	for(int l=0; l<lastSiteWithDataPlusOne; ++l){
		rho[l][l] = 1.0;

		//init Q
		for(int b1=0; b1<numStates_B; ++b1){
			Q[b1][b1] = 1.0;
			for(int b2=b1+1; b2<numStates_B; ++b2){
				Q[b1][b2] = 0.0;
				Q[b2][b1] = 0.0;
			}
		}

		//freq at L
		freqAtL = 0.0;
		for(int b1=1; b1<numStates_B; b1 = b1 + 2)
			freqAtL += pi[l][b1];

		//next locus
		for(int l2=l+1; l2<lastSiteWithDataPlusOne; ++l2){
			//swap Q
			tmpQ = Q;
			Q = Q_old;
			Q_old = tmpQ;

			//fill Q
			for(int b1=0; b1<numStates_B; ++b1){
				for(int b2=0; b2<numStates_B; ++b2){
					Q[b1][b2] = Q_old[b1][previousState[b2][0]] * r[l2][previousState[b2][0]][b2 % 2] + Q_old[b1][previousState[b2][1]] * r[l2][previousState[b2][1]][b2 % 2];
				}
			}

			//calculate correlation
			D = 0.0;
			freqAtL2 = 0.0;
			for(int b1=1; b1<numStates_B; b1 = b1 + 2){
				freqAtL2 += pi[l2][b1];
				for(int b2=1; b2<numStates_B; b2 = b2 + 2){
					D += pi[l][b1] * (Q[b1][b2] - pi[l2][b2]);
				}
			}
			//rho[l][l2] = D / sqrt(variance[l] * variance[l2]);
			rho[l][l2] = D / sqrt(variance[l] * variance[l2]);
			rho[l][l2] *= rho[l][l2];
			rho[l2][l] = rho[l][l2];
		}
	}

	//print to file
	std::string filename = outputName + "_correlations.txt.gz";
	gz::ogzstream outfile(filename.c_str());
	outfile << "-";
	for(int l=0; l<lastSiteWithDataPlusOne; ++l){
		outfile << "\tL" << l+1;
	}
	outfile << "\n";
	for(int l=0; l<lastSiteWithDataPlusOne; ++l){
		outfile << "L" << l+1;
		for(int l2=0; l2<lastSiteWithDataPlusOne; ++l2){
			outfile << "\t" << rho[l][l2];
		}
		outfile << "\n";
	}
	outfile.close();
}





















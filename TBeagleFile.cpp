/*
 * TBeagleFile.cpp
 *
 *  Created on: Feb 23, 2016
 *      Author: phaentu
 */


#include "TBeagleFile.h"

TBeagleFile::TBeagleFile(){
	myStream = NULL;
	myOutStream = NULL;
	eof = false;
	streamOpend = false;
	outputStreamOpend = false;
	isZipped = false;
	sampleNames = NULL;
	currentLine = 0;
	numSamplesInFile = 0;
}

TBeagleFile::TBeagleFile(std::string & filename, bool zipped):TBeagleFile(){
	initialize(filename, zipped);
}

void TBeagleFile::initialize(std::string & filename, bool zipped){
	//open stream
	if(zipped) myStream = new gz::igzstream(filename.c_str());
	else myStream = new std::ifstream(filename.c_str());
	if(!(*myStream)) throw "Failed to open file '" + filename + "'!";
	streamOpend = true;

	//read header
	if(!readLine(header)) throw "Failed to read header from Beagle File!";
	numSamplesInFile = (header.size() - 3) / 3;
	if(numSamplesInFile < 1) throw "Beagle file appears not to contain any individuals!";
	sampleNames = new std::string[numSamplesInFile];
	for(int i=0; i<numSamplesInFile; ++i)
		sampleNames[i] = header[3*(i+1)];
	currentLine = 0;
}

bool TBeagleFile::readLine(std::vector<std::string> & thisVec){
	thisVec.clear();
	do{
		if(myStream->eof() || !myStream->good()){
			eof=true;
			return false;
		}
		fillVectorFromLineWhiteSpaceSkipEmpty(*myStream, thisVec);
	} while(thisVec.empty());
	++currentLine;
	return true;
}

void TBeagleFile::openOutputStream(std::string & filename, bool zipped){
	if(zipped) myOutStream = new gz::ogzstream(filename.c_str());
	else myOutStream = new std::ofstream(filename.c_str());
	if(!(*myOutStream)) throw "Failed to open file '" + filename + "'!";
	outputStreamOpend = true;
}

bool TBeagleFile::next(){
	return readLine(vec);
}

void TBeagleFile::writeHeader(){
	it = header.begin();
	(*myOutStream) << *it;
	++it;
	for(; it != header.end(); ++it){
		(*myOutStream) << "\t" << *it;
	}
	(*myOutStream) << "\n";
}

void TBeagleFile::writeLine(){
	it = vec.begin();
	(*myOutStream) << *it;
	++it;
	for(; it != vec.end(); ++it){
		(*myOutStream) << "\t" << *it;
	}
	(*myOutStream) << "\n";
}

void TBeagleFile::fillGenotypeLikelihoods(double** genotypeLikelihoods){
	//use data stored in vec
	int index = 0;
	int g;
	for(int i=0; i<numSamplesInFile; ++i){
		index += 3;
		for(g=0; g<3; ++g){
			genotypeLikelihoods[i][g] = stringToDouble(vec[index + g]);
		}
	}
}

std::string TBeagleFile::getChrPosString(){
	//return chr and pos of current marker
	//->guess from maker. Assume chr_pos notation
	//if there is no _, then chr = 0 and pos = marker
	std::string::size_type pos = vec[0].find_first_of('_');
	if(pos == std::string::npos){
		return "0\t" + vec[0];
	} else {
		tmp = vec[0];
		tmp2 = tmp.substr(0, pos) + "\t";
		tmp.erase(0,2);
		return tmp2 + tmp;
	}
}

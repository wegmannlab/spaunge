/*
 * commonfunctions.h
 *
 *  Created on: Nov 4, 2016
 *      Author: wegmannd
 */

#ifndef COMMONFUNCTIONS_H_
#define COMMONFUNCTIONS_H_

#include <math.h>
#include <iostream>

void fillInitialEstimateOfGenotypeFrequencies(double* genoFreq, int & numSamples, double** genotypeLikelihoods);
void estimateGenotypeFrequenciesNullModel(double* genotypeFrequencies, int & numSamples, double** genotypeLikelihoods, double epsilonF);



#endif /* COMMONFUNCTIONS_H_ */

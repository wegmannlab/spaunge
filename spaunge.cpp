/*
 * spaunge.cpp
 *
 *  Created on: Feb 19, 2016
 *      Author: wegmannd
 */

#include <sys/time.h>
#include "TSpaunge_core.h"

//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
	struct timeval start, end;
    gettimeofday(&start, NULL);

	TLog logfile;
	logfile.newLine();
	logfile.write(" SpAUnGe 1.0 ");
	logfile.write("*************");
    try{
		//read parameters from the command line
    	TParameters myParameters(argc, argv, &logfile);

		//verbose?
		bool verbose=myParameters.parameterExists("verbose");
		if(!verbose) logfile.listNoFile("Running in silent mode (use 'verbose' to get a status report on screen)");
		logfile.setVerbose(verbose);


		//open log file that handles the output
		std::string  logFilename=myParameters.getParameterString("logFile", false);
		if(logFilename.length()>0){
			logfile.openFile(logFilename.c_str());
			logfile.writeFileOnly(" SpAUnGe 1.0 ");
			logfile.writeFileOnly("*************");
		}

		//create genome object
		TSpaunge_core core(&logfile, myParameters);

		//what to do?
		std::string task = myParameters.getParameterStringWithDefault("task", "associate");
		if(task == "simulate"){
			logfile.startIndent("Simulating GWAS NGS data:");
			core.simulateGWASData(myParameters);
		} else if(task == "associate"){
			logfile.startIndent("SPotting Associations from UNcertain GEnotypes:");
			core.spotAssociations(myParameters);
		} else if(task == "filter"){
			logfile.startIndent("Filtering input file on MAF:");
			core.filterOnFrequency(myParameters);
		} else if(task == "inferLinkage"){
			logfile.startIndent("Inferring De-Bruijn-type Linkage Model:");
			core.inferLinkageModel(myParameters);
		} else throw "Unknown task '" + task + "'!";
		logfile.clearIndent();

		//write unsused parameters
		std::string unusedParams=myParameters.getListOfUnusedParameters();
		if(unusedParams!=""){
			logfile.newLine();
			logfile.warning("The following parameters were not used: " + unusedParams + "!");
		}
    }
	catch (std::string & error){
		logfile.error(error);
	}
	catch (const char* error){
		logfile.error(error);
	}
	catch(std::exception & error){
		logfile.error(error.what());
	}
	catch (...){
		logfile.error("unhandeled error!");
	}
	logfile.clearIndent();
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logfile.list("Program terminated in ", runtime, " min!");
	logfile.close();
    return 0;
}







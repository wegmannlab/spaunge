/*
 * TSpaunge_core.cpp
 *
 *  Created on: Feb 19, 2016
 *      Author: wegmannd
 */


#include "TSpaunge_core.h"

//-------------------------------------------------------
//TSpaunge_core
//-------------------------------------------------------
TSpaunge_core::TSpaunge_core(TLog* Logfile, TParameters & params){
	logfile = Logfile;
	initializeRandomGenerator(params);

	outputName = params.getParameterStringWithDefault("out", "spaunge");
};

//----------------------------------------------------------------------------------------
void TSpaunge_core::initializeRandomGenerator(TParameters & params){
	logfile->listFlush("Initializing random generator ...");

	if(params.parameterExists("fixedSeed")){
		randomGenerator=new TRandomGenerator(params.getParameterLong("fixedSeed"), true);
	} else if(params.parameterExists("addToSeed")){
		randomGenerator=new TRandomGenerator(params.getParameterLong("addToSeed"), false);
	} else randomGenerator=new TRandomGenerator();
	logfile->write(" done with seed " + toString(randomGenerator->usedSeed) + "!");
	randomGeneratorInitialized = true;
}

//----------------------------------------------------------------------------------------
void TSpaunge_core::prepareInput(TParameters & params){
	isVCF = false;
	isBeagle = false;
	std::string filename;

	//open input stream
	if(params.parameterExists("vcf")){
		filename = params.getParameterString("vcf");
		logfile->list("Reading VCF from file '" + filename + "'");
		vcfFile.openStream(filename, false);
		logfile->conclude("Found data for " + toString(vcfFile.numSamples()) + " individuals.");
		isVCF = true;
		isZipped = false;
	} else if(params.parameterExists("zvcf")){
		filename = params.getParameterString("zvcf");
		logfile->list("Reading zipped VCF from file '" + filename + "'");
		vcfFile.openStream(filename, true);
		logfile->conclude("Found data for " + toString(vcfFile.numSamples()) + " individuals.");
		isVCF = true;
		isZipped = true;
	} else if(params.parameterExists("beagle")){
		filename = params.getParameterString("beagle");
		logfile->list("Reading genotype likelihoods in BEAGLE format from file '" + filename + "'");
		beagleFile.initialize(filename, false);
		logfile->conclude("Found data for " + toString(beagleFile.numSamples()) + " individuals.");
		isBeagle = true;
		isZipped = false;
	} else if(params.parameterExists("zbeagle")){
		filename = params.getParameterString("zbeagle");
		logfile->list("Reading genotype likelihoods in zipped BEAGLE format from file '" + filename + "'");
		beagleFile.initialize(filename, true);
		logfile->conclude("Found data for " + toString(beagleFile.numSamples()) + " individuals.");
		isBeagle = true;
		isZipped = true;
	} else throw "No input file specified! Use either vcf, zvcf, beagle or zbeagle.";

	//read header of VCF
	if(isVCF){
		//enable parsers
		vcfFile.enablePositionParsing();
		//vcfFile.enableFormatParsing();
		//vcfFile.enableSampleParsing();
		vcfFile.enableVariantParsing();

		//read VCF header
		logfile->listFlush("Reading header of a VCF file in '" + vcfFile.fileFormat + "' format ... ");
		vcfFile.parseHeaderVCF_4_0();
		logfile->write(" done!");
		logfile->conclude("File contains " + toString(vcfFile.numSamples()) + " samples.");
	}

	//update outputName, if not given
	if(outputName == "spaunge") outputName = extractBeforeLast(filename, '.');
}

//----------------------------------------------------------------------------------------
//Simulations
//----------------------------------------------------------------------------------------
void TSpaunge_core::simulateGWASData(TParameters & params){
	int numInd = params.getParameterIntWithDefault("ind", 100);
	logfile->startIndent("Will simulate genetic data for " + toString(numInd) + "individual(s) with the following parameters:");

	//read basic params
	long seqLength = params.getParameterLongWithDefault("seqLen", 10000);
	logfile->list("The Total sequence length will be " + toString(seqLength));
	int readLength = params.getParameterIntWithDefault("readLen", 100);
	logfile->list("Will simulate reads of length " + toString(readLength));
	double coverage = params.getParameterDoubleWithDefault("coverage", 5.0);
	long numReads = (double) seqLength / (double) readLength * coverage;
	logfile->list("Will simulate " + toString(numReads) + " reads to obtain an average coverage of " + toString(coverage));
	bool variableCoverage = params.parameterExists("variableCoverage");

	//read base frequencies
	std::vector<double> tmp;
	double baseFreqs[4];
	double baseFreqsCumul[4];
	std::string freqs = params.getParameterStringWithDefault("baseFreqs", "0.25,0.25,0.25,0.25");
	fillVectorFromStringAnySkipEmptyCheck(freqs, tmp, ",");
	if(tmp.size() != 4) throw "Unable to understand base frequencies '" + freqs + "'!";
	double sum = 0.0;
	for(int i=0; i<4; ++i) sum += tmp[i];
	if(sum <= 0.0) throw "Unable to understand base frequencies '" + freqs + "'!";
	double cumulSum = 0.0;
	for(int i=0; i<4; ++i){
		baseFreqs[i] = tmp[i] / sum;
		if(baseFreqs[i] > 0.999) throw "At least two bases need to have a frequency > 0.001!";
		baseFreqsCumul[i] = baseFreqs[i] + cumulSum;
		cumulSum += baseFreqs[i];
	}
	baseFreqsCumul[3] = 1.0;
	logfile->list("Will simulate bases A,C,G and T with frequencies " + toString(baseFreqs[0]) + ", " + toString(baseFreqs[1]) + ", " + toString(baseFreqs[2]) + " and " + toString(baseFreqs[3]) + ", respectively");

	//diversity: #snps and SNP frequencies
	long numSnps = params.getParameterLongWithDefault("numSNPs", 100);
	double snpFreq = params.getParameterDoubleWithDefault("SNPFreq", 0.5);
	logfile->list("Will simulate " + toString(numSnps) + " SNPs with a population frequency of " + toString(snpFreq) + " each");
	if(numSnps > seqLength) throw "Can not simulate more SNPs than the sequence is long!";
	if(numSnps < 1) throw "There needs to be at least 1 SNP!";

	//sequencing errors: normal distribution on qualities
	double meanQual = params.getParameterDoubleWithDefault("meanQual", 30.0);
	double sdQual = params.getParameterDoubleWithDefault("sdQual", 10.0);
	logfile->list("Will simulate base qualities from a normal distribution with mean " + toString(meanQual) + " and sd " + toString(sdQual));
	logfile->endIndent();

	//phenotypes
	int numPhenotypes =  params.getParameterIntWithDefault("numPheno", 1);
	if(numPhenotypes > numSnps) throw "There can not be more phenotypes than SNPs!";
	logfile->startIndent("Will simulate data for " + toString(numPhenotypes) + " phenotype(s) with the following parameters:");

	//variables for phenotypes
	//TODO: find better way to read parameters: use explvar for all cases!
	double phenotypeBeta0 = params.getParameterDoubleWithDefault("beta0", 0.0);
	double phenotypeBeta1 = params.getParameterDoubleWithDefault("beta1", 1.0);
	double phenotypeBeta2 = params.getParameterDoubleWithDefault("beta2", 0.0);
	double phenotypeErrorVariance = params.getParameterDoubleWithDefault("errorVar", 1.0);
	double phenotypeErrorStd = sqrt(phenotypeErrorVariance);

	//add distinction of models
	bool isLogit = false;
	bool isGeneralized = false;
	std::string model = params.getParameterStringWithDefault("model", "additive");
	if(model == "additive"){
		if(params.parameterExists("logit")){
			logfile->startIndent("Simulating a logit additive model with the following parameters:");
			isLogit = true;
		} else {
			logfile->startIndent("Simulating an additive model with the following parameters:");
			if(params.parameterExists("explVar")){
				double explainedVariance = params.getParameterDouble("explVar");
				double phenoVar = params.getParameterDoubleWithDefault("phenoVar", 1.0);
				logfile->list("phenotypic variance = " + toString(phenoVar));
				logfile->list("explained variance = " + toString(explainedVariance));
				phenotypeErrorVariance = phenoVar * (1.0 - explainedVariance);
				phenotypeBeta1 = sqrt(phenoVar * explainedVariance / (2.0 * snpFreq * (1.0 - snpFreq)));
				phenotypeErrorStd = sqrt(phenotypeErrorVariance);
			}
		}
	} else if(model == "generalized"){
		if(params.parameterExists("logit")){
			logfile->startIndent("Simulating a logit generalized model with the following parameters:");
			isLogit = true;
			isGeneralized = true;
		} else {
			logfile->startIndent("Simulating a generalized model with the following parameters:");
			isGeneralized = true;
		}
	} else throw "Unknown model '" + model + "'!";

	logfile->list("beta0 = " + toString(phenotypeBeta0));
	logfile->list("beta1 = " + toString(phenotypeBeta1));
	if(isGeneralized)
		logfile->list("beta2 = " + toString(phenotypeBeta1));
	if(!isLogit)
		logfile->list("error variance = " + toString(phenotypeErrorVariance));

	//logfile->list("Additive genetic effects on phenotype with explained variance = " + toString(phenotypeBeta1));
	//logfile->conclude("beta1 = " + toString(phenotypeBeta1));

	logfile->endIndent();

	//make sure outputname is not empty
	if(outputName == ""){
		outputName = "GWAS_simulation";
	}
	std::string filename;


	//1) simulate reference sequence as A, C, G, or T
	//-----------------------------------------------
	logfile->listFlush("Simulating reference sequence ...");
	char* refSeq = new char[seqLength];
	double rand;
	for(int i=0; i<seqLength; ++i){
		rand = randomGenerator->getRand();
		if(rand > baseFreqsCumul[2]) refSeq[i] = bases[3];
		else if(rand > baseFreqsCumul[1]) refSeq[i] = bases[2];
		else if(rand > baseFreqsCumul[0]) refSeq[i] = bases[1];
		else refSeq[i] = bases[0];
	}
	logfile->write(" done!");

	//writing as FASTA to file
	filename = outputName + "_refSeq.fasta";
	logfile->listFlush("Writing reference sequence to '" + filename + "' ...");
	std::ofstream outfile(filename.c_str());
	if(!outfile) throw "Failed to open SAM file '" + filename + "'!";
	outfile << ">1\n";
	//write always 70 characters per line
	long index = 0;
	while(index < seqLength){
		outfile << refSeq[index];
		++index;
		if(index % 70 == 0){
			outfile << "\n";
		}
	}
	outfile << "\n";
	outfile.close();
	logfile->write(" done!");


	//2) simulate SNPs
	//----------------
	//first: location
	long* snpLocations = new long[numSnps];
	bool* isSnp = new bool[seqLength];
	long* snpIndex = new long[seqLength];
	for(long i=0; i<seqLength; ++i){
		isSnp[i] = false;
		snpIndex[i] = -1;
	}
	long remaining = numSnps;
	index = 0;
	double prob;
	for(long i=0; i<seqLength; ++i){
		rand = randomGenerator->getRand();
		prob = (double) remaining / (double) (seqLength - i);
		if(rand < prob){
			snpLocations[index] = i;
			isSnp[i] = true;
			snpIndex[i] = index;
			--remaining;
			++index;
			if(remaining == 0) break;
		}
	}

	//now get alternative alleles
	char* altAllele = new char[numSnps];
	long loc;
	for(index = 0; index < numSnps; ++index){
		loc = snpLocations[index];
		altAllele[index] = refSeq[loc];
		while(altAllele[index] == refSeq[loc]){
			//choose new allele according to base frequencies
			rand = randomGenerator->getRand();
			if(rand > baseFreqsCumul[2]) altAllele[index] = bases[3];
			else if(rand > baseFreqsCumul[1]) altAllele[index] = bases[2];
			else if(rand > baseFreqsCumul[0]) altAllele[index] = bases[1];
			else altAllele[index] = bases[0];
		}
	}

	//simulating phenotypic effects: choose SNP for each phenotype
	long* snpsAffectingPhenotype = new long[numPhenotypes];
	bool** isPhenotypicSnp = new bool*[numPhenotypes];
	for(int p=0; p<numPhenotypes; ++p){
		isPhenotypicSnp[p] = new bool[numSnps];
		for(long i=0; i<numSnps; ++i)
			isPhenotypicSnp[p][i] = false;
	}
	remaining = numPhenotypes;
	index = 0;

	for(long i=0; i<numSnps; ++i){
		rand = randomGenerator->getRand();
		prob = (double) remaining / (double) (numSnps - i);
		if(rand < prob){
			snpsAffectingPhenotype[index] = snpLocations[i];
			isPhenotypicSnp[index][i] = true;
			--remaining;
			++index;
			if(remaining == 0) break;
		}
	}

	//write SNPs to file: chr pos ref alt
	filename = outputName + "_snps.txt";
	logfile->listFlush("Writing simulated SNPs to '" + filename + "' ...");
	outfile.open(filename.c_str());
	if(!outfile) throw "Failed to open SAM file '" + filename + "'!";
	outfile << "Chr\tPos\tRef\tAlt";
	for(int p=0; p<numPhenotypes; ++p)
		outfile << "\tpheno" << p;
	outfile << "\n";
	for(index = 0; index < numSnps; ++index){
		outfile << "1\t" << snpLocations[index] + 1 << "\t" << refSeq[snpLocations[index]] << "\t" << altAllele[index];
		for(int p=0; p<numPhenotypes; ++p){
			if(isPhenotypicSnp[p][index]) outfile << "\t1";
			else  outfile << "\t0";
		}
		outfile << "\n";
	}
	outfile.close();
	logfile->write(" done!");


	//4) Open phenotype file
	filename = outputName + "_phenotypes.txt";
	logfile->list("Will write simulated phenotypes '" + filename + "'");
	std::ofstream phenoFile(filename.c_str());
	if(!phenoFile) throw "Failed to open file '" + filename + "'!";
	phenoFile << "Individual";
	for(int i=0; i<numPhenotypes; ++i) phenoFile << "\tPhenotype" << i;
	phenoFile << "\n";


	//3) now loop over individuals
	//----------------------------
	//prepare variables
	std::vector<long> readStart;
	char* chr1 = new char[seqLength];
	char* chr2 = new char[seqLength];
	double errorRate;
	char* qualities = new char[readLength];
	int l,q, NM_tag;
	char* allele;
	char error;
	double pheno;
	int rl; //actual read length

	//simulate genotypes
	double cumulGenoFreq[3];
	cumulGenoFreq[0] = (1.0 - snpFreq) * (1.0 - snpFreq);
	cumulGenoFreq[1] = cumulGenoFreq[0] + 2.0 * snpFreq * (1.0 - snpFreq);
	cumulGenoFreq[2] = 1.0;

	//loop
	logfile->startNumbering("Simulating data for " + toString(numInd) + " individual(s):");
	for(int ind = 0; ind < numInd; ++ind){
		logfile->number("Simulating data for individual " + toString(ind) + ":"); logfile->addIndent();

		//simulate the two chromosomes
		for(long i=0; i<seqLength; ++i){
			if(isSnp[i]){
				//simulate genotype
				rand = randomGenerator->getRand();
				if(rand > cumulGenoFreq[1]){
					//homo ref
					chr1[i] = refSeq[i];
					chr2[i] = refSeq[i];
				} else if(rand > cumulGenoFreq[0]){
					//hetero
					if(randomGenerator->getRand() < 0.5){
						chr1[i] = refSeq[i];
						chr2[i] = altAllele[snpIndex[i]];
					} else {
						chr1[i] = altAllele[snpIndex[i]];
						chr2[i] = refSeq[i];
					}
				} else {
					//homo alt
					chr1[i] = altAllele[snpIndex[i]];
					chr2[i] = chr1[i];
				}
			} else {
				chr1[i] = refSeq[i];
				chr2[i] = refSeq[i];
			}
		}

		//open SAM file for writing
		filename = outputName + "_Ind";
		if(ind < 1000) filename += '0';
		if(ind < 100) filename += '0';
		if(ind < 10) filename += '0';
		filename += toString(ind) + ".sam";
		logfile->list("Writing data to '" + filename + "'");
		outfile.open(filename.c_str());
		if(!outfile)
			throw "Failed to open SAM file '" + filename + "'!";

		//write SAM header
		outfile << "@HD	VN:1.4	GO:none	SO:coordinate\n";
		outfile << "@SQ	SN:1	LN:" << seqLength << "\n";
		outfile << "@RG	ID:Shotgun_Mi_Hi_" << ind + 1<< "\tPL:ILLUMINA\tPU:UNKNOWN\tLB:UNKNOWN\tSM:Ind" << ind << "\tCN:MAINZ\n";

		//simulate reads
		//first start positions
		readStart.clear();
		if(variableCoverage){
			for(long i=0; i<numReads; ++i)
				readStart.push_back(randomGenerator->getRand((long) -readLength + 1L, seqLength));
			std::sort(readStart.begin(), readStart.end());
		} else {
			//create "stair" of reads shifted by
			int shift = readLength / coverage;
			for(long i=shift-readLength; i<seqLength; i=i+shift){
				readStart.push_back(i);
			}
			std::cout << std::endl;
		}

		//now simulate all those reads!
		for(std::vector<long>::iterator it=readStart.begin(); it!=readStart.end(); ++it){
			//adjust read length based on overhang
			rl = readLength;
			if(*it < 0){
				rl += *it;
				*it = 0;
			}
			if(*it + rl > seqLength){
				rl -= (*it + rl) - seqLength;
			}

			//write mandatory fields
			outfile << "*\t0\t1\t" << *it + 1 << "\t50\t" << rl << "M\t*\t0\t0\t";

			//pick allele
			if(randomGenerator->getRand() < 0.5) allele = chr1;
			else allele = chr2;

			//simulate qualities & errors and write bases
			l = 0;
			NM_tag = 0;  //distance between read and reference sequence
			for(long i=*it; i< *it + rl; ++i, ++l){
				q = round(randomGenerator->getNormalRandom(meanQual, sdQual));
				if(q<0) q = 0;
				if(q>42) q = 42;
				qualities[l] = q + 33;
				errorRate = pow(10.0, (double) q / -10.0); //dephred
				if(randomGenerator->getRand() < errorRate){
					//add error
					error = bases[randomGenerator->getRand(0, 4)];
					while(error == allele[i])
						error = bases[randomGenerator->getRand(0, 4)];
					outfile << error;
					if(error != refSeq[i]) NM_tag++;
				}else{
					outfile << allele[i];
					if(allele[i] != refSeq[i]) NM_tag++;
				}
			}

			//write qualities
			outfile << "\t";
			outfile.write(qualities, rl);
			outfile << "\tNM:i:" << NM_tag;
			outfile << "\tRG:Z:Shotgun_Mi_Hi_Ind" << ind;
			outfile << "\n";
		}

		//done!
		outfile.close();
		logfile->endIndent();

		//simulate phenotypes
		phenoFile << "Ind" << ind;
		for(int p=0; p<numPhenotypes; ++p){
			if(isGeneralized){
				if(chr1[snpsAffectingPhenotype[p]] == refSeq[snpsAffectingPhenotype[p]]){
					if(chr2[snpsAffectingPhenotype[p]] == refSeq[snpsAffectingPhenotype[p]]) pheno = phenotypeBeta0;
					else pheno = phenotypeBeta1;
				} else {
					if(chr2[snpsAffectingPhenotype[p]] == refSeq[snpsAffectingPhenotype[p]]) pheno = phenotypeBeta1;
					else pheno = phenotypeBeta2;
				}
			} else {
				pheno = phenotypeBeta0;
				if(chr1[snpsAffectingPhenotype[p]] != refSeq[snpsAffectingPhenotype[p]]){
					pheno += phenotypeBeta1;
				}
				if(chr2[snpsAffectingPhenotype[p]] != refSeq[snpsAffectingPhenotype[p]]){
					pheno += phenotypeBeta1;
				}
			}
			if(isLogit){
				pheno = 1.0 / (1.0 + exp(-pheno));
				if(randomGenerator->getRand() < pheno) pheno = 1.0;
				else pheno = 0.0;
			} else {
				pheno += randomGenerator->getNormalRandom(0.0, phenotypeErrorStd);
			}
			phenoFile << "\t" << pheno;
		}
		phenoFile << "\n";
	} // end looping over individuals

	//clean up
	phenoFile.close();
	delete[] refSeq;
	delete[] snpLocations;
	delete[] isSnp;
	delete[] snpIndex;
	delete[] altAllele;

	delete[] snpsAffectingPhenotype;
	for(int p=0; p<numPhenotypes; ++p)
		delete[] isPhenotypicSnp[p];
	delete[] isPhenotypicSnp;
	delete[] chr1;
	delete[] chr2;
	delete[] qualities;
}

//----------------------------------------------------------------------------------------
//Inference
//----------------------------------------------------------------------------------------
void TSpaunge_core::printProgress(long lines, long & tests, struct timeval & start){
	struct timeval end;
	gettimeofday(&end, NULL);
	float runtime = (end.tv_sec  - start.tv_sec)/60.0;
	logfile->list("Parsed " + toString(lines) + " lines and performed " + toString(tests) + " association tests in " + toString(runtime) + " min");
}

void TSpaunge_core::spotAssociationsVCF(TPhenotypes* phenotypes, double & freqFilter, int & progressFrequency){
	logfile->startIndent("Parsing through VCF file:");
	struct timeval start;
	gettimeofday(&start, NULL);
	long numAssociationTestPerformed = 0;

	//parse...
	while(vcfFile.next()){
		//only look at positions in VCF with at least two alleles that are not 'N'
		if(vcfFile.getNumAlleles() > 1){
			if(vcfFile.getFirstAltAllele() != "<NON_REF>"){
				//do association test
				vcfFile.parseFormat();
				vcfFile.parseSamples();
				if(phenotypes->performAssociationTest(vcfFile, freqFilter))
					++numAssociationTestPerformed;
			}
		}

		//report progress
		if(vcfFile.currentLine % progressFrequency == 0)
			printProgress(vcfFile.currentLine, numAssociationTestPerformed, start);
	}
	//report progress
	printProgress(vcfFile.currentLine, numAssociationTestPerformed, start);
	logfile->endIndent();
}

void TSpaunge_core::spotAssociationsBeagle(TPhenotypes* phenotypes, double & freqFilter, int & progressFrequency){
	logfile->startIndent("Parsing through BEAGLE file:");
	struct timeval start;
	gettimeofday(&start, NULL);
	long numAssociationTestPerformed = 0;

	//parse...
	while(beagleFile.next()){
		if(phenotypes->performAssociationTest(beagleFile, freqFilter))
			++numAssociationTestPerformed;

		//report progress
		if(beagleFile.getCurrentLine() % progressFrequency == 0)
			printProgress(beagleFile.getCurrentLine(), numAssociationTestPerformed, start);
	}
	//report progress
	printProgress(beagleFile.getCurrentLine(), numAssociationTestPerformed, start);
	logfile->endIndent();
}

void TSpaunge_core::spotAssociations(TParameters & params){
	//open vcf or beagle file
	prepareInput(params);

	//alternative model
	TPhenotypes* phenotypes;
	std::string tmp = params.getParameterStringWithDefault("model", "additive");
	outputName += "_" + tmp;
	if(tmp == "additive"){
		if(params.parameterExists("logit")){
			logfile->list("Will test for associations using an additive logit model.");
			outputName += "_logit";
			//phenotypes = new TPhenotypesLogit(outputName);
			throw "Not yet implemented!";
		} else {
			logfile->list("Will test for associations using an additive model.");
			if(params.parameterExists("MLEGenotypes"))
				phenotypes = new TPhenotypesAdditiveMLEGenotypes(outputName);
			else
				phenotypes = new TPhenotypes(outputName);
		}
	} else if(tmp == "generalized"){
		if(params.parameterExists("logit")){
			logfile->list("Will test for associations using a generalized logit model.");
			outputName += "_logit";
			//phenotypes = new TPhenotypesLogit(outputName);
			throw "Not yet implemented!";
		} else {
			logfile->list("Will test for associations using a generalized model.");
			phenotypes = new TPhenotypesGeneralized(outputName);
		}
	} else throw "Unknown alternative model '" + tmp + "'!";

	//read phenotypes
	std::string phenoFileName = params.getParameterString("pheno");
	logfile->listFlush("Reading phenotypes from file '" + phenoFileName + "' ...");
	if(isVCF){
		phenotypes->initialize(phenoFileName, vcfFile);
	} else if(isBeagle){
		phenotypes->initialize(phenoFileName, beagleFile);
	} else throw "Unknown input format!";
	logfile->write(" done!");
	logfile->conclude("Read data of " + toString(phenotypes->getNumSamples()) + " individuals for " + toString(phenotypes->getNumPhenotypes()) + " phenotype(s).");
	if(phenotypes->getNumSamples() < 5) throw "Can not perform association tests: data for less than 5 individuals!";

	//variables
	double epsilonLL = params.getParameterDoubleWithDefault("epsLL", 0.0001);
	double epsilonF = params.getParameterDoubleWithDefault("epsF", 0.0001);
	phenotypes->setConvergenceThresholds(epsilonLL, epsilonF);
	int progressFrequency = params.getParameterIntWithDefault("reportFreq", 100);
	double freqFilter = -1.0;
	if(params.parameterExists("freqFilter")){
		freqFilter = params.getParameterDouble("freqFilter");
	}

	//parse...
	if(isVCF) spotAssociationsVCF(phenotypes, freqFilter, progressFrequency);
	else if(isBeagle) spotAssociationsBeagle(phenotypes, freqFilter, progressFrequency);
	else throw "Unknown file type!";

	//done!
	delete phenotypes;
}

//----------------------------------------------------------------------------------------
//filter files on frequency
//----------------------------------------------------------------------------------------
void TSpaunge_core::printProgressFrequencyFiltering(long lines, long & numRetainedLoci, struct timeval & start){
	struct timeval end;
	gettimeofday(&end, NULL);
	float runtime = (end.tv_sec  - start.tv_sec)/60.0;
	logfile->list("Parsed " + toString(lines) + " lines and retained " + toString(numRetainedLoci) + " loci in " + toString(runtime) + " min");
}

void TSpaunge_core::filterOnFrequencyVCF(double & epsilonF, double & mafFilter, int & progressFrequency, gz::ogzstream & mafFile){
	//open new VCF file for writing
	std::string filename = outputName + "_filtered.vcf.gz";
	logfile->list("Writing filtered VCF file to '" + filename + "'");
	vcfFile.openOutputStream(filename, true);
	vcfFile.writeHeaderVCF_4_0();

	//parse through VCF
	logfile->startIndent("Parsing through VCF file:");
	struct timeval start;
	gettimeofday(&start, NULL);
	long numAcceptedLoci = 0;

	//variables / create storage
	int numSamples = vcfFile.numSamples();
	double** genotypeLikelihoods = new double*[numSamples];
	for(int i=0; i<numSamples; ++i)
		genotypeLikelihoods[i] = new double[3];
	double* genotypeFrequencies = new double[3];
	double f;

	//parse...
	while(vcfFile.next()){
		//only look at positions in VCF with at least two alleles that are not 'N'
		if(vcfFile.getNumAlleles() > 1){
			if(vcfFile.getFirstAltAllele() != "<NON_REF>"){
				//read genotype likelihoods of first two alleles from file
				vcfFile.parseFormat();
				vcfFile.parseSamples();
				for(int i=0; i<numSamples; ++i){
					vcfFile.fillGenotypeLikelihoods(i, genotypeLikelihoods[i]);
				}

				//estimate frequency
				estimateGenotypeFrequenciesNullModel(genotypeFrequencies, numSamples, genotypeLikelihoods, epsilonF);
				f = genotypeFrequencies[0] + 0.5 * genotypeFrequencies[1];
				if(f > 0.5) f = 1.0 - f;
				mafFile << vcfFile.chr() << "\t" << vcfFile.position() << "\t" << f << "\n";

				//write to new VCF if filter passed
				if(f >= mafFilter){
					vcfFile.writeLine();
					++numAcceptedLoci;
				}
			}
		}

		//report progress
		if(vcfFile.currentLine % progressFrequency == 0)
			printProgressFrequencyFiltering(vcfFile.currentLine, numAcceptedLoci, start);
	}
	//report progress
	printProgressFrequencyFiltering(vcfFile.currentLine, numAcceptedLoci, start);
	logfile->endIndent();

	//clean up
	for(int i=0; i<numSamples; ++i)
		delete[] genotypeLikelihoods[i];
	delete[] genotypeLikelihoods;
	delete[] genotypeFrequencies;
	mafFile.close();
}

void TSpaunge_core::filterOnFrequencyBeagle(double & epsilonF, double & mafFilter, int & progressFrequency, gz::ogzstream & mafFile){
	//open new Beagle file for writing
	std::string filename = outputName + "_filtered.beagle";
	logfile->list("Writing filtered Beagle file to '" + filename + "'");
	beagleFile.openOutputStream(filename, true);
	beagleFile.writeHeader();

	//parse through VCF
	logfile->startIndent("Parsing through Beagle file:");
	struct timeval start;
	gettimeofday(&start, NULL);
	long numAcceptedLoci = 0;

	//variables / create storage
	int numSamples = beagleFile.numSamples();
	double** genotypeLikelihoods = new double*[numSamples];
	for(int i=0; i<numSamples; ++i)
		genotypeLikelihoods[i] = new double[3];
	double* genotypeFrequencies = new double[3];
	double f;

	//parse...
	while(beagleFile.next()){
		beagleFile.fillGenotypeLikelihoods(genotypeLikelihoods);

		//estimate frequency
		estimateGenotypeFrequenciesNullModel(genotypeFrequencies, numSamples, genotypeLikelihoods, epsilonF);
		f = genotypeFrequencies[0] + 0.5 * genotypeFrequencies[1];
		if(f > 0.5) f = 1.0 - f;
		mafFile << beagleFile.getChrPosString() << "\t" << f << "\n";

		//write to new VCF if filter passed
		if(f >= mafFilter){
			beagleFile.writeLine();
			++numAcceptedLoci;
		}

		//report progress
		if(beagleFile.getCurrentLine() % progressFrequency == 0)
			printProgressFrequencyFiltering(beagleFile.getCurrentLine(), numAcceptedLoci, start);
	}
	//report progress
	printProgressFrequencyFiltering(beagleFile.getCurrentLine(), numAcceptedLoci, start);
	logfile->endIndent();

	//clean up
	for(int i=0; i<numSamples; ++i)
		delete[] genotypeLikelihoods[i];
	delete[] genotypeLikelihoods;
	delete[] genotypeFrequencies;
}

void TSpaunge_core::filterOnFrequency(TParameters & params){
	//open vcf or beagle file
	prepareInput(params);

	//variables
	double epsilonF = params.getParameterDoubleWithDefault("epsF", 0.0001);
	int progressFrequency = params.getParameterIntWithDefault("reportFreq", 10000);
	double freqFilter = params.getParameterDouble("minMAF");
	if(freqFilter <= 0.0 || freqFilter >= 1.0)
		throw "MAF filter not within allowed range (0.0,1.0)!";

	//open file to write frequencies to
	std::string filename = outputName + "_MAF.txt.gz";
	logfile->list("Will write estimated minor allele frequencies to file '" + filename + "'");
	gz::ogzstream mafFile(filename.c_str());
	if(!mafFile) throw "Failed to open file '" + filename + "' for writing!";

	//now parse file
	if(isVCF){
		filterOnFrequencyVCF(epsilonF, freqFilter, progressFrequency, mafFile);
	} else if(isBeagle){
		filterOnFrequencyBeagle(epsilonF, freqFilter, progressFrequency, mafFile);
	} else throw "Unknown file type!";

	//done!
	mafFile.close();
}

//----------------------------------------------------------------------------------------
//Linkage Model
//----------------------------------------------------------------------------------------
void TSpaunge_core::inferLinkageModel(TParameters & params){
	//open vcf or beagle file
	prepareInput(params);

	//estimate linkage model
	TLinkageModelEstimator* linkageEstimator;
	if(isVCF){
		linkageEstimator = new TLinkageModelEstimator(params, logfile, &vcfFile);
	} else if(isBeagle){
		linkageEstimator = new TLinkageModelEstimator(params, logfile, &beagleFile);
	} else throw "Unknown input format!";

	//now estimate!
	linkageEstimator->estimate(outputName);

}








